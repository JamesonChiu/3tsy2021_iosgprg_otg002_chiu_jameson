﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
    public string name;
   // public int HP;
    public int moveSpeed;
    public int ammo;
    //public int firerate;
    public float reloadSpeed;

    // public HP_Bar healthBar;

    public Weapon Gun;
    public static Unit Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public void init(string nName, int nHealth, int nMoveSpeed)
    {
        this.name = nName;
        //this.HP = nHealth;
        this.moveSpeed = nMoveSpeed;

        if(this.GetComponent<Health>())
        {
            this.GetComponent<Health>().Init(nHealth);
        }
    }

    public void Fire()
    {
        Pistol.Instance.GetComponent<Pistol>().BulletSpawnPistol();
        //Gun.Shoot();
        // Debug.Log("FIRE");
        //Gun.GetComponent<Pistol>().StartCoroutine(BulletSpawnPistol());
    }

    public void Reload()
    {
        Debug.Log("Reload");
        Pistol.Instance.currentAmmo = Pistol.Instance.clip_size;
        Pistol.Instance.max_ammo -= Pistol.Instance.clip_size;
    }

    public void TakeDamage(int damage)
    {
        this.GetComponent<Health>().Damaged(damage);
       
    }

    public void Death()
    {

    }
}
