﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MaxAmmo_UI : MonoBehaviour
{

    public Text max_Ammo;

    public Player player;
    // Start is called before the first frame update
    void Start()
    {
        max_Ammo = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if(player.Secondary == Weapons.Pistol)
        {
            max_Ammo.text = Pistol.Instance.max_ammo.ToString(); 
        }
    }
}
