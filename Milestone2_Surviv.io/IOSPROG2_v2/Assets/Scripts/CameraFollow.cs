﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform player;

    public float camera_speed = 10f;
    [SerializeField] Vector3 offset;

    private void LateUpdate()
    {
        offset = new Vector3(0, 0, -50);
        transform.position = player.position + offset;

    }
}
