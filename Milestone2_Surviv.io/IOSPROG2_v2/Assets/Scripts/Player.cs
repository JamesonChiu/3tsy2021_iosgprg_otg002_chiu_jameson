﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Unit
{
    public Joystick controller;
    public Joystick aim;

    public Rigidbody2D rb;

    float H_Movement = 0f;
    float V_Movement = 0f;

    public Weapons Primary;
    public Weapons Secondary;

    [SerializeField] int Pistol_Ammo_Inventory;

    public GameObject pistol_equip;
    //public GameObject equipWeapon;

    Vector2 Movement;
    Vector3 Directionals;
    Vector3 Joystick_Directionals;

    [SerializeField] bool isFiring;
    //[SerializeField] bool isReloading;
    private void Start()
    {
        this.GetComponent<Player>().init("Player", 100, 500);
        Primary = Weapons.None;
        Secondary = Weapons.None;
       
        isFiring = false;
        //isReloading = false;
        // healthBar.setMaxHP(HP);

        pistol_equip.SetActive(false);
    }

    void Update()
    {
        H_Movement = controller.Horizontal * moveSpeed;
        V_Movement = controller.Vertical * moveSpeed;

        Movement = new Vector2(H_Movement, V_Movement);

        this.rb.freezeRotation = true;

        Joystick_Directionals = new Vector3(aim.Horizontal, aim.Vertical, 0); //if joystick moves thru x and y
        if (Joystick_Directionals != new Vector3(0,0,0)) 
        {
            transform.eulerAngles = new Vector3(0, 0, (Mathf.Atan2(aim.Vertical, aim.Horizontal)* Mathf.Rad2Deg) - 90f);
            this.rb.freezeRotation = false;
           // Fire();
            
            if(pistol_equip.activeSelf != false)
            {
                //pistol_equip.GetComponent<Pistol>().BulletSpawnPistol();
                Fire();
            }
           
           
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            TakeDamage(10);
            Debug.Log("Damaged");
        }

      

        if (this.Secondary == Weapons.Pistol && Pistol.Instance.currentAmmo <= 0)
        {
           
            Debug.Log("Reload");
            Reload();
        }

      
    }

    private void FixedUpdate()
    {
        rb.velocity = Movement * Time.fixedDeltaTime;
      
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Pistol"))
        {
            //equip pistol
            Debug.Log("Pistol");
            Secondary = Weapons.Pistol;
            pistol_equip.SetActive(true);
            Destroy(collision.gameObject);
            WeaponSpawner.Instance.RemoveToList_Weapon(collision.gameObject);

            if(Secondary == Weapons.Pistol)
            {
                Pistol.Instance.max_ammo += 7;
            }

        }

        else if (collision.gameObject.CompareTag("Shotgun"))
        {
            //equip Shotgun
            Debug.Log("Shotgun");
            Primary = Weapons.Shotgun;
            Destroy(collision.gameObject);
            WeaponSpawner.Instance.RemoveToList_Weapon(collision.gameObject);
        }

        else if (collision.gameObject.CompareTag("AR"))
        {
            //equip AR
            Debug.Log("AR");
            Primary = Weapons.AR;
            Destroy(collision.gameObject);
            WeaponSpawner.Instance.RemoveToList_Weapon(collision.gameObject);
        }

        else if (collision.gameObject.CompareTag("Pistol_Bullet"))
        {
            //Add Pistol Ammo
            Debug.Log("Pistol_Ammo");
            if(Secondary == Weapons.Pistol)
            {
                Pistol.Instance.max_ammo += 10;
                if (Pistol_Ammo_Inventory >= 0 && Secondary == Weapons.Pistol)
                {
                    Pistol.Instance.max_ammo += Pistol_Ammo_Inventory;
                }

            }
            else if (Secondary == Weapons.None)
            {
                Pistol_Ammo_Inventory += 10;
            }

            
            Destroy(collision.gameObject);
            
        }
        else if (collision.gameObject.CompareTag("Shotgun_Ammo"))
        {
            //Add Shotgun_Ammo
            Destroy(collision);
        }
        else if (collision.gameObject.CompareTag("AR_Ammo"))
        {
            //Add AR_Ammo
            Destroy(collision);
        }



    }
}









/* Directionals = new Vector3(0,0,Mathf.Atan(aim.Vertical/aim.Horizontal) * 360);
         transform.rotation = Quaternion.Euler(Directionals);*/

/*Directionals = new Vector3(transform.position.x + aim.Horizontal, transform.position.y + aim.Vertical, Mathf.Atan2(aim.Horizontal, aim.Vertical) * Mathf.Rad2Deg);
// Vector3 LookPos = transform.position + Directionals;
this.transform.rotation = Quaternion.LookRotation(Directionals);
           // this.transform.up = Joystick_Directionals;*/


/*Debug.Log(collision.gameObject.name);
       pistol_equip = collision.gameObject;
       collision.gameObject.transform.SetParent(this.gameObject.transform);
       collision.transform.position = pistol_equip.transform.position;
      */

