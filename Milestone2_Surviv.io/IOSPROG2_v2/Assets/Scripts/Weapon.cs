﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum Weapons { Pistol, Shotgun, AR, None }
public enum Ammo { pistolAmmo, shotgunAmmo, ARAmmo }


public class Weapon : MonoBehaviour
{

    public static Weapon Instance;

    public string Weapon_Name;
    public int clip_size;
    public int currentAmmo;
    public int max_ammo;
    public int fireRate;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public void Weapon_Init(string wName, int wClip_size, int wCurrent_ammo, int wMax_ammo, int wFireRate)
    {

        this.Weapon_Name = wName;
        this.clip_size = wClip_size;
        this.currentAmmo = wCurrent_ammo;
        this.max_ammo = wMax_ammo;
        this.fireRate = wFireRate;

    }

    public void Shoot()
    {
        
        Debug.Log("Fire");
    }
}
