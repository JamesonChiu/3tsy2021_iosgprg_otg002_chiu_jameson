﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pistol_Ammo : Weapon_Ammo
{
    // public int bullet_Speed;
    // [SerializeField] float timer; 

   

    private void Awake()
    {
        this.GetComponent<Pistol_Ammo>().Ammo_Init("Pistol_Ammo", 500);
    }
    private void Update()
    {

        //this.transform.Translate(Vector3.up * Time.deltaTime * bullet_Speed);

        if (timer >= 500)
        {
           Destroy(this.gameObject);
           timer = 0;
        }
        timer++;
    }



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            //this.gameObject.SetActive(false);
            Destroy(this.gameObject);
            collision.gameObject.SetActive(false);
        }

        else if (collision.gameObject.CompareTag("Enemy"))
        {
            // this.gameObject.SetActive(false);
            Destroy(this.gameObject);
            collision.gameObject.SetActive(false);
        }

        else if (collision.gameObject.CompareTag("Obstacle"))
        {
            Destroy(this.gameObject);
        }


    }
}
