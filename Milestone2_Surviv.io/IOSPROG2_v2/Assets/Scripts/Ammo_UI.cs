﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ammo_UI : MonoBehaviour
{
    // Start is called before the first frame update

    public Text Current_Ammo;
   

    public Player player;
    void Start()
    {
        Current_Ammo = GetComponent<Text>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if(player.Secondary == Weapons.Pistol)
        {
          Current_Ammo.text = Pistol.Instance.currentAmmo.ToString();
        }
       
    }
}
