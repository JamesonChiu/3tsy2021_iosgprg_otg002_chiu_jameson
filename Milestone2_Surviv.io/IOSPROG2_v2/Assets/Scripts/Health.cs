﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] int currentHP { get; set; }
    [SerializeField] int MaxHP;
   // public int maxHP { get { return MaxHP; } }

    public HP_Bar healthBar;

    public void Init(int maxHealth)
    {
        this.MaxHP = maxHealth;
        currentHP = MaxHP;
        healthBar.setMaxHP(MaxHP);
    }

   public void Damaged(int damage)
    {
        currentHP -= damage;
        healthBar.setHP(currentHP);
    }

}
