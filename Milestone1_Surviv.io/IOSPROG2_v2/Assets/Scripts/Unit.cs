﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
    public string name;
    public int HP;
    public int moveSpeed;
    public int ammo;
    public int firerate;
    public float reloadSpeed;

    public HP_Bar healthBar;

    public void init(string nName, int nHealth, int nMoveSpeed)
    {
        this.name = nName;
        this.HP = nHealth;
        this.moveSpeed = nMoveSpeed;
    }

    public void Fire()
    {
        Debug.Log("FIRE");
    }

    public void Reload(int reloadSpeed)
    {

    }

    public void TakeDamage(int damage)
    {
        HP -= damage;
        healthBar.setHP(HP);
       
    }

    public void Death()
    {

    }
}
