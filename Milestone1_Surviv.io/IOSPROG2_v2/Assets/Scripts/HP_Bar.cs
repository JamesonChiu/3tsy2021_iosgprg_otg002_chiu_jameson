﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HP_Bar : MonoBehaviour
{

    public Slider slider;
    

    public void Start()
    {
       
    }


    public void setMaxHP(int health)
    {
        slider.maxValue = health;
        slider.value = health;

    }

    public void setHP(int health)
    {
        slider.value = health;
        if(slider.value > 100)
        {
            slider.value = 100;
        }
    }
}
