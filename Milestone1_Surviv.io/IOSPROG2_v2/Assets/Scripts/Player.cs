﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Unit
{
    public Joystick controller;
    public Joystick aim;

    public Rigidbody2D rb;

    float H_Movement = 0f;
    float V_Movement = 0f;

    Vector2 Movement;
    Vector3 Directionals;
    Vector3 Joystick_Directionals;

    private void Start()
    {
        this.GetComponent<Player>().init("Player", 100, 500);

        healthBar.setMaxHP(HP);

        
    }

    void Update()
    {
        H_Movement = controller.Horizontal * moveSpeed;
        V_Movement = controller.Vertical * moveSpeed;

        Movement = new Vector2(H_Movement, V_Movement);

        this.rb.freezeRotation = true;

        Joystick_Directionals = new Vector3(aim.Horizontal, aim.Vertical, 0); //if joystick moves thru x and y
        if (Joystick_Directionals != new Vector3(0,0,0)) 
        {
            transform.eulerAngles = new Vector3(0, 0, (Mathf.Atan2(aim.Vertical, aim.Horizontal)* Mathf.Rad2Deg) - 90f);
            this.rb.freezeRotation = false;
            Fire();
        }


        if (Input.GetKeyDown(KeyCode.Space))
        {
            TakeDamage(10);
            Debug.Log("Space");
        }

       
    }

    private void FixedUpdate()
    {
        rb.velocity = Movement * Time.fixedDeltaTime;
      
    }
}









/* Directionals = new Vector3(0,0,Mathf.Atan(aim.Vertical/aim.Horizontal) * 360);
         transform.rotation = Quaternion.Euler(Directionals);*/

/*Directionals = new Vector3(transform.position.x + aim.Horizontal, transform.position.y + aim.Vertical, Mathf.Atan2(aim.Horizontal, aim.Vertical) * Mathf.Rad2Deg);
// Vector3 LookPos = transform.position + Directionals;
this.transform.rotation = Quaternion.LookRotation(Directionals);
           // this.transform.up = Joystick_Directionals;*/

