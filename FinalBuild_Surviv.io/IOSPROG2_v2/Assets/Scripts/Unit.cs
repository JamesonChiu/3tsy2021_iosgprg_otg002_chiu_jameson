﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Unit : MonoBehaviour
{
    public string name;
    public int moveSpeed;
    public int ammo;
    public float reloadSpeed;
    public GameObject[] Slots;
    public int CurrentEquip;
    public Rigidbody2D rb;

    public void init(string nName, int nHealth, int nMoveSpeed)
    {
        this.name = nName;
        this.moveSpeed = nMoveSpeed;

        if(this.GetComponent<Health>())
        {
            this.GetComponent<Health>().Init(nHealth);
        }
    }

    public void Firing()
    {
       
        this.Slots[CurrentEquip].GetComponent<Weapon>().Shoot();
        
    }

    public void Reloading()
    {
        Debug.Log("Reload");
        Pistol.Instance.currentAmmo = Pistol.Instance.clip_size;
        Pistol.Instance.max_ammo -= Pistol.Instance.clip_size;
    }

    public void TakeDamage(int damage)
    {
        this.GetComponent<Health>().Damaged(damage);
       
    }

    public void Death()
    {
        
    }
}
