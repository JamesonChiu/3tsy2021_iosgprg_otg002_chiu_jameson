﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class MenuMgr : Singleton<MenuMgr>
{
    

    [SerializeField] List<MenuMainCanvas> menuCanvasList = new List<MenuMainCanvas>();

    public void RegisterMenu(MenuMainCanvas menuCanvas)
    {
        menuCanvasList.Add(menuCanvas);
        menuCanvas.Hide();
    }

    public void HideAll()
    {
        foreach (MenuMainCanvas menuCanvas in menuCanvasList)
        {
            menuCanvas.Hide();
        }
    }

    public void ShowCanvas(MenuType typeMenu)
    {
        HideAll();

        foreach(MenuMainCanvas menuCanvas in menuCanvasList)
        {
            if(menuCanvas.menuType == typeMenu)
            {
                menuCanvas.Show();
                break;
            }
        }
            
    }
}
