﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(HP_Bar))]

public class Player : Unit
{
    public float H_Movement = 0f;
    public float V_Movement = 0f;

    public GameObject[] guns = new GameObject[3];

    public Vector2 Movement;
    public Vector3 Directionals;
    public Vector3 Joystick_Directionals;

    public HP_Bar healthBar;

    public static Player Instance;
    private void Start()
    {
        this.GetComponent<Player>().init("Player", 100, 500);
        CurrentEquip = 0;
        GameMgr.Instance.player = this;
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        this.gameObject.GetComponent<Health>().act += healthBar.setMaxHP;
        this.gameObject.GetComponent<Health>().dmg += healthBar.setHP;

    }

    void Update()
    {

        if (this.Slots[CurrentEquip] != null && Slots[CurrentEquip].GetComponent<Weapon>().max_ammo < 0)
        {
            Slots[CurrentEquip].GetComponent<Weapon>().currentAmmo = 0;
            noAmmo();
        }
        else if (this.Slots[CurrentEquip] != null && Slots[CurrentEquip].GetComponent<Weapon>().currentAmmo <= 0)
        {
            Debug.Log("Reload");
            Slots[CurrentEquip].GetComponent<Weapon>().Reload();
           
        }
     
        
        if (this.GetComponent<Health>().currentHP <= 0)
        {
            Death();
        }
    }

    public void noAmmo()
    {
     if (guns[1].activeSelf == true) // Shotgun
     {
        guns[1].SetActive(false);
     }
     else if(guns[2].activeSelf == true) // AR
     {
        guns[2].SetActive(false);
     }
     else if (guns[0].activeSelf == true) // Pistol
     {
        guns[0].SetActive(false);
     }
       
    }

    private void FixedUpdate()
    {
        rb.velocity = Movement * Time.fixedDeltaTime;
      
    }

    

    private void OnTriggerEnter2D(Collider2D collision) // PLAYER PICKUP
    {
        if(collision.gameObject.CompareTag("Shotgun"))
        {
            if (guns[0].activeSelf == true)
            {
                guns[1].SetActive(false); // set sprite false
            }
            else
            {
                guns[1].SetActive(true); // set sprite true
            }

            if (guns[2].activeSelf == true)
            {
                guns[2].SetActive(false);
            }

            Slots[0] = guns[1]; // set sprite as equip weapon and slot

            if (Slots[1] == null) // If pistol slot is empty
            {
                CurrentEquip = 0; //equip ShotGun
            }
           

            if (Slots[0].CompareTag(collision.gameObject.tag) || CurrentEquip == 1) // PRIMARY
            {
                Slots[0].GetComponent<Weapon>().max_ammo += Slots[0].GetComponent<Weapon>().clip_size;
              
            }

            Destroy(collision.gameObject);
            WeaponSpawner.Instance.RemoveToList_Weapon(collision.gameObject);
        }

        else if (collision.gameObject.CompareTag("AR"))
        {
            if (guns[0].activeSelf == true)
            {
                guns[2].SetActive(false); // set sprite false
            }
            else
            {
                guns[2].SetActive(true); // set sprite true
            }

            if (guns[1].activeSelf == true)
            {
                guns[1].SetActive(false);
            }

            Slots[0] = guns[2]; // set sprite as equip weapon and slot

            if (Slots[1] == null)
            {
                CurrentEquip = 0;
            }
           

            if (Slots[0].CompareTag(collision.gameObject.tag) || CurrentEquip == 1) // PRIMARY
            {
                Slots[0].GetComponent<Weapon>().max_ammo += Slots[0].GetComponent<Weapon>().clip_size;

            }

            Destroy(collision.gameObject);
            WeaponSpawner.Instance.RemoveToList_Weapon(collision.gameObject);
        }

        else if(collision.gameObject.CompareTag("Pistol"))
        {        
            if(guns[1].activeSelf == true || guns[2].activeSelf == true)
            {
                guns[0].SetActive(false); // set sprite false
            }
            else
            {
                guns[0].SetActive(true); // set sprite true
            }

            Slots[1] = guns[0]; // set sprite as equip weapon and slot
            if(Slots[0] == null)
            {
                CurrentEquip = 1;
            }
           
            if (Slots[1].CompareTag(collision.gameObject.tag) || CurrentEquip == 0) // SECONDARY
            {
                Slots[1].GetComponent<Weapon>().max_ammo += Slots[1].GetComponent<Weapon>().clip_size;

            }

            Destroy(collision.gameObject);
            WeaponSpawner.Instance.RemoveToList_Weapon(collision.gameObject);
        }

        else if (collision.gameObject.CompareTag("Pistol_Bullet"))
        {
            if (Slots[1].CompareTag("Pistol") || CurrentEquip == 0) // SECONDARY
            {
                Debug.Log("Pistol_Ammo");
                Slots[1].GetComponent<Weapon>().max_ammo += 14;

            }

            Destroy(collision.gameObject);
            WeaponSpawner.Instance.RemoveToList_Ammo(collision.gameObject);
        }
        else if (collision.gameObject.CompareTag("Shotgun_Bullet"))
        {
            if (Slots[0].CompareTag("Shotgun") || CurrentEquip == 1) // PRIMARY
            {
                Slots[0].GetComponent<Weapon>().max_ammo += 4;

            }
            Destroy(collision.gameObject);
            WeaponSpawner.Instance.RemoveToList_Ammo(collision.gameObject);
        }
        else if (collision.gameObject.CompareTag("AR_Bullet"))
        {
            if (Slots[0].CompareTag("AR") || CurrentEquip == 1) // PRIMARY
            {
                Slots[0].GetComponent<Weapon>().max_ammo += 60;

            }
            Destroy(collision.gameObject);
            WeaponSpawner.Instance.RemoveToList_Ammo(collision.gameObject);
        }


    }
}