﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pistol_Ammo : Weapon_Ammo
{
    private void Awake()
    {
        this.GetComponent<Pistol_Ammo>().Ammo_Init("Pistol_Ammo", 500);
    }
    private void Update()
    {
        if (timer >= 300)
        {
            Destroy(this.gameObject);
            timer = 0;
        }
        timer++;

    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(this.gameObject);
            collision.gameObject.GetComponent<Player>().TakeDamage(5);
        }

        else if (collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(this.gameObject);
            collision.gameObject.GetComponent<Enemy>().TakeDamage(5);
        }

        else if (collision.gameObject.CompareTag("Obstacle"))
        {
            Destroy(this.gameObject);
        }

        else if (collision.gameObject.CompareTag("WorldBound"))
        {
            Destroy(this.gameObject);
        }

    }
}
