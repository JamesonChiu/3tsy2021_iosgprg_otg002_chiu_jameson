﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMgr : Singleton<GameMgr>
{
    // Start is called before the first frame update

    public Player player { get; set; }

    void Start()
    {
        StartCoroutine(AsyncLoadScene("MainMenu",OnMenuLoaded));
       
    }

    void OnMenuLoaded()
    {
        MenuMgr.Instance.ShowCanvas(MenuType.MainMenu);
       
    }

    public void OnGameLoaded()
    {
        MenuMgr.Instance.ShowCanvas(MenuType.InGame);
    }

    IEnumerator AsyncLoadScene (string name, Action onCallback = null)
    {
        AsyncOperation asyncLoadScene = SceneManager.LoadSceneAsync(name, LoadSceneMode.Additive);

        while (!asyncLoadScene.isDone)
        {
            yield return null;
        }

        Debug.Log("Add Scene");

        if(onCallback != null)
        {
            onCallback.Invoke();
        }
    }

    IEnumerator AsyncUnLoadScene(string name, Action onCallback = null)
    {
        AsyncOperation UnloadSceneAsync = SceneManager.UnloadSceneAsync(name);

        while (!UnloadSceneAsync.isDone)
        {
            yield return null;
        }

        Debug.Log("Unload Scene");

        if (onCallback != null)
        {
            onCallback.Invoke();
        }
    }

    public void StartGame()
    {
        StartCoroutine(AsyncLoadScene("SampleScene", OnGameLoaded)); // inGame
    }

    public void OnRestart() // Load
    {
        StartCoroutine(AsyncLoadScene("SampleScene", OnGameLoaded));
    }

    public void ResetGame() // Unload
    { 
        StartCoroutine(AsyncUnLoadScene("SampleScene", OnRestart));
    }

    public void GameOver()
    {
       MenuMgr.Instance.ShowCanvas(MenuType.GameOver);
    }
}

