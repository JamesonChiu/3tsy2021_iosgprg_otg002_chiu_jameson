﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Weapon : MonoBehaviour
{
    public enum Weapons { Pistol, Shotgun, AR, None }
    public enum Ammo { pistolAmmo, shotgunAmmo, ARAmmo }

    public Ammo bulletType;

    public static Weapon Instance;

    public string Weapon_Name;
    public int clip_size;
    public int currentAmmo;
    public int max_ammo;
    public float fireRate;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public void Weapon_Init(string wName, int wClip_size, int wCurrent_ammo, int wMax_ammo, float wFireRate)
    {

        this.Weapon_Name = wName;
        this.clip_size = wClip_size;
        this.currentAmmo = wCurrent_ammo;
        this.max_ammo = wMax_ammo;
        this.fireRate = wFireRate;

    }

    public void Reload()
    {
        //Debug.Log("Reload");
        currentAmmo = clip_size;
        max_ammo -= clip_size;

    }

    public virtual void Shoot()
    {   
       // Debug.Log("Firing");
    }
}
