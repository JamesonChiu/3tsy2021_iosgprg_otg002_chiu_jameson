﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : Unit
{
    public float waitTime;
    public float startWaitTime;

    public float stoppingDistance;

    public Animator animator;

    Vector2 WanderTarget;

    public Transform playerTarget;

    public Transform pistolTarget;
    public Transform shotgunTarget;
    public Transform ARTarget;

    public Transform LootTarget;

    public Vector2 moveSpots;
    public Collider2D[] colliders;

    public int index;

    public Rigidbody2D Enemy_rb;

    public GameObject[] guns = new GameObject[3];


    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Enemy>().init("Enemy", 100, 10);
        waitTime = startWaitTime;

        moveSpots = new Vector2(UnityEngine.Random.Range(-100f, 100f), UnityEngine.Random.Range(-55f, 55f));

        playerTarget = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    public void PickUp()
    {

    }
        
    public void AI_Attack()
    {
        if (Slots[CurrentEquip].activeSelf != false)
        {
            Firing();
        }
    }

    public void Update()
    {
        animator.SetFloat("Distance", Vector3.Distance(transform.position, playerTarget.position));

        if(this.GetComponent<Enemy>().CurrentEquip == 1)
        {
            this.GetComponent<Enemy>().CurrentEquip = 0;
            guns[0].SetActive(false); 
            if(this.GetComponent<Enemy>().Slots[0].gameObject.CompareTag("Shotgun"))
            {
                guns[0].SetActive(true);
            }
            else if(this.GetComponent<Enemy>().Slots[0].gameObject.CompareTag("AR"))
            {
                guns[0].SetActive(true);
            }
        }

        EnemyLoot();

        if(this.GetComponent<Health>().currentHP <= 0)
        {
            if (this.GetComponent<Enemy>().Slots[0] != null)
            {
                this.GetComponent<Enemy>().Slots[0] = null;
                if (guns[1].activeSelf == true)
                {
                    guns[1].SetActive(false); 
                }
                else if (guns[2].activeSelf == true)
                {
                    guns[2].SetActive(false);
                }
            }
            this.gameObject.SetActive(false);
            Vector3 newPos = new Vector3(UnityEngine.Random.Range(-100f, 100f), UnityEngine.Random.Range(-55f, 55f), 0);
            this.transform.position = newPos;
            this.gameObject.SetActive(true);
            this.GetComponent<Enemy>().init("Enemy", 100, 10);
            
        }

       if(this.Slots[CurrentEquip] != null && Slots[CurrentEquip].GetComponent<Weapon>().currentAmmo <= 0)
       {
            Debug.Log("Reload");
            Slots[CurrentEquip].GetComponent<Weapon>().Reload();
       }

    }

    public void EnemyLoot()
    {
        colliders = Physics2D.OverlapCircleAll(this.transform.position, 10.0f);


        if (LootTarget != null)
        {
            animator.SetFloat("LootDistance", Vector3.Distance(transform.position, LootTarget.position));

        }
        if (colliders.Length > 1)
        {

            if (LootTarget == null)
            {
                index = UnityEngine.Random.Range(1, colliders.Length);
                animator.SetFloat("LootDistance", 999);
            }

            if ((!colliders[index].gameObject.CompareTag("Pistol") && LootTarget == null || !colliders[index].gameObject.CompareTag("Shotgun") && LootTarget == null || !colliders[index].gameObject.CompareTag("AR") && LootTarget == null))
            {
                index = UnityEngine.Random.Range(1, colliders.Length);


                if (colliders[index].gameObject.CompareTag("Pistol") && LootTarget == null || colliders[index].gameObject.CompareTag("Shotgun") && LootTarget == null || colliders[index].gameObject.CompareTag("AR") && LootTarget == null)
                {
                    LootTarget = colliders[index].gameObject.transform.root.gameObject.transform;
                   
                }
            }
            Debug.Log(colliders[index].gameObject.transform.root.gameObject.transform);
           

        }
    }

    private void OnTriggerEnter2D(Collider2D collision) // ENEMY PICKUP
    {
        if (collision.gameObject.CompareTag("Shotgun"))
        {
            if (guns[0].activeSelf == true)
            {
                guns[1].SetActive(false); // set sprite false
            }
            else
            {
                guns[1].SetActive(true); // set sprite true
            }

            if (guns[2].activeSelf == true)
            {
                guns[2].SetActive(false);
            }

            Slots[0] = guns[1]; // set sprite as equip weapon and slot

            if (Slots[1] == null)
            {
                CurrentEquip = 0;
            }
            else
            {
                CurrentEquip = 1;
            }


            if (Slots[0].CompareTag(collision.gameObject.tag) || CurrentEquip == 1) // PRIMARY
            {
                Slots[0].GetComponent<Weapon>().max_ammo += Slots[0].GetComponent<Weapon>().clip_size;

            }

            Destroy(collision.gameObject);
            WeaponSpawner.Instance.RemoveToList_Weapon(collision.gameObject);
        }

        else if (collision.gameObject.CompareTag("AR"))
        {
            if (guns[0].activeSelf == true)
            {
                guns[2].SetActive(false); // set sprite false
            }
            else
            {
                guns[2].SetActive(true); // set sprite true
            }

            if (guns[1].activeSelf == true)
            {
                guns[1].SetActive(false);
            }

            Slots[0] = guns[2]; // set sprite as equip weapon and slot

            if (Slots[1] == null)
            {
                CurrentEquip = 0;
            }
            else
            {
                CurrentEquip = 1;
            }

            if (Slots[0].CompareTag(collision.gameObject.tag) || CurrentEquip == 1) // PRIMARY
            {
                Slots[0].GetComponent<Weapon>().max_ammo += Slots[0].GetComponent<Weapon>().clip_size;

            }

            Destroy(collision.gameObject);
            WeaponSpawner.Instance.RemoveToList_Weapon(collision.gameObject);
        }

        else if (collision.gameObject.CompareTag("Pistol"))
        {
            if (guns[1].activeSelf == true || guns[2].activeSelf == true)
            {
                guns[0].SetActive(false); // set sprite false
            }
            else
            {
                guns[0].SetActive(true); // set sprite true
            }

            Slots[1] = guns[0]; // set sprite as equip weapon and slot

            if (Slots[0] == null)
            {
                CurrentEquip = 1;
            }
            else
            {
                CurrentEquip = 0;
            }

            if (Slots[1].CompareTag(collision.gameObject.tag) || CurrentEquip == 0) // SECONDARY
            {
                Slots[1].GetComponent<Weapon>().max_ammo += Slots[1].GetComponent<Weapon>().clip_size;

            }

            Destroy(collision.gameObject);
            WeaponSpawner.Instance.RemoveToList_Weapon(collision.gameObject);
        }

    }

}

