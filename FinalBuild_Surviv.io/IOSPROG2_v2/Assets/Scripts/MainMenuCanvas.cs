﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuCanvas : MenuMainCanvas
{
    public void StartGame()
    {
        //MenuMgr.Instance.ShowCanvas(MenuType.InGame);
        GameMgr.Instance.StartGame();
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
