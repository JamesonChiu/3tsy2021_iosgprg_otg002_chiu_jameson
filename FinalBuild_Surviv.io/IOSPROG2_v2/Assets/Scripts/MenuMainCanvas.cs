﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum MenuType
{
    MainMenu,
    InGame,
    GameOver,
}



public class MenuMainCanvas : MonoBehaviour
{
    public MenuType menuType;
    
    protected virtual void Start()
    {
        MenuMgr.Instance.RegisterMenu(this);
    }

    public void Show()
    {
        if(!this.gameObject.activeSelf)
        {
            this.gameObject.SetActive(true);
        }
    }

    public void Hide()
    {
        if (this.gameObject.activeSelf)
        {
            this.gameObject.SetActive(false);
        }
    }

}
