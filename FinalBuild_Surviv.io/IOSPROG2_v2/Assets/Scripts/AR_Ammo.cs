﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AR_Ammo : Weapon_Ammo
{
    private void Awake()
    {
        this.GetComponent<AR_Ammo>().Ammo_Init("Ar_Ammo", 35);
    }
    private void Update()
    {
        if (timer >= 150)
        {
            Destroy(this.gameObject);
            timer = 0;
        }
        timer++;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(this.gameObject);
            collision.gameObject.GetComponent<Player>().TakeDamage(10);
        }

        else if (collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(this.gameObject);
            collision.gameObject.GetComponent<Enemy>().TakeDamage(10);
        }

        else if (collision.gameObject.CompareTag("Obstacle"))
        {
            Destroy(this.gameObject);
        }

        else if (collision.gameObject.CompareTag("WorldBound"))
        {
            Destroy(this.gameObject);
        }

    }
}
