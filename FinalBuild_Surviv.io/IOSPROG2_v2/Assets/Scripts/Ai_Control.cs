﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ai_Control : StateMachineBehaviour
{

    public GameObject AI_Entity;
    public Transform target;

    public float speedRot;
    public float speed;
    public float accuracy;

    public GameObject pistolLoot;
    public GameObject shotgunLoot;
    public GameObject ARLoot;

    public Transform LootPistol;
    public Transform LootShotgun;
    public Transform LootAR;


    // Start is called before the first frame update
    void Start()
    {
        speedRot = 1;
        speed = 2;
        accuracy = 3;

    }

   
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        AI_Entity = animator.gameObject;
        pistolLoot = animator.gameObject;
        shotgunLoot = animator.gameObject;
        ARLoot = animator.gameObject;
        target = AI_Entity.GetComponent<Enemy>().playerTarget;
        LootPistol = AI_Entity.GetComponent<Enemy>().pistolTarget;
        LootShotgun = AI_Entity.GetComponent<Enemy>().shotgunTarget;
        LootAR = AI_Entity.GetComponent<Enemy>().ARTarget;
    }
}
