﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pistol : Weapon
{
    // Start is called before the first frame update

    public GameObject PistolAmmo;
    public Transform Pistol_Nuzzle;
    public Weapons weaponType;
    [SerializeField] float ShotTime;

    public Transform pistolTarget;
    void Start()
    {
        this.GetComponent<Pistol>().Weapon_Init("Pistol",7, 7, 14, 1);
        ShotTime = 0f;

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void Shoot()
    {
        if(Time.time >= ShotTime)
        {
            
            GameObject newBullet = Instantiate(PistolAmmo, Pistol_Nuzzle.position, Pistol_Nuzzle.rotation);
            newBullet.GetComponent<Rigidbody2D>().velocity = Pistol_Nuzzle.up * Time.fixedDeltaTime * newBullet.GetComponent<Pistol_Ammo>().bullet_Speed;
            ShotTime = Time.time + fireRate;
            currentAmmo--;
        }
    }
}
