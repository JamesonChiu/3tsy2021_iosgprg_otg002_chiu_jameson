﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class WeaponSpawner : MonoBehaviour
{

    public static WeaponSpawner Instance;

    public GameObject[] Guns;
    public GameObject[] Ammo;

    [SerializeField] List<GameObject> weaponList = new List<GameObject>();
    [SerializeField] List<GameObject> ammoList = new List<GameObject>();

    
    private void Start()
    {
        StartCoroutine(Weapon_Spawn());
        StartCoroutine(Ammo_Spawn());
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void AddToList_Weapon(GameObject weapon)
    {
        weaponList.Add(weapon);
    }

    public void RemoveToList_Weapon(GameObject removeWeapon)
    {
        weaponList.Remove(removeWeapon);
    }

    

    void AddToList_Ammo(GameObject ammo)
    {
        ammoList.Add(ammo);
    }

    public void RemoveToList_Ammo(GameObject removeAmmo)
    {
        ammoList.Remove(removeAmmo);
    }

    IEnumerator Weapon_Spawn()
    {
        while (true)
        {
            yield return new WaitForSeconds(2);
            int RNG_Weapon = Random.Range(0,3);
            Vector3 Random_Pos = new Vector3(Random.Range(-100f, 100f), Random.Range(-55f, 55f), 0);
            GameObject newWeapon = Instantiate(Guns[RNG_Weapon], Random_Pos, Quaternion.identity);
            SceneManager.MoveGameObjectToScene( newWeapon, SceneManager.GetSceneByName("SampleScene"));
            AddToList_Weapon(newWeapon);
        }
    }

    IEnumerator Ammo_Spawn()
    {
        while (true)
        {
            yield return new WaitForSeconds(2);
            int RNG_Ammo = Random.Range(0, 3);
            Vector3 Random_Pos = new Vector3(Random.Range(-100f, 100f), Random.Range(-55f, 55f), 0);
            GameObject newAmmo = Instantiate(Ammo[RNG_Ammo], Random_Pos, Quaternion.identity);
            SceneManager.MoveGameObjectToScene(newAmmo, SceneManager.GetSceneByName("SampleScene"));
            AddToList_Ammo(newAmmo);
        }
    }

}



