﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Loot : Ai_Control
{
  //  OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
    }

  //  OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
       AI_Entity.GetComponent<Enemy>().transform.position = Vector2.MoveTowards(AI_Entity.GetComponent<Enemy>().transform.position, AI_Entity.GetComponent<Enemy>().LootTarget.position, AI_Entity.GetComponent<Enemy>().moveSpeed * Time.deltaTime);
       AI_Entity.GetComponent<Enemy>().transform.up = AI_Entity.GetComponent<Enemy>().LootTarget.position - AI_Entity.GetComponent<Enemy>().transform.position;
        
    }

  //  OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
    }

   
}
