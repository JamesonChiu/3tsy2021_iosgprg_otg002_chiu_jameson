﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Manager : MonoBehaviour
{
    public void primarySlotButton()
    {
        if(Player.Instance.Slots[1] != null)
        {
            Player.Instance.Slots[1].SetActive(false);
        }
        Player.Instance.CurrentEquip = 0;
        Player.Instance.Slots[Player.Instance.CurrentEquip].SetActive(true);
    }

    public void secondarySlotButton()
    {
        if (Player.Instance.Slots[0] != null)
        {
            Player.Instance.Slots[0].SetActive(false);
        }
        Player.Instance.CurrentEquip = 1;
        Player.Instance.Slots[Player.Instance.CurrentEquip].SetActive(true);
    }

}
