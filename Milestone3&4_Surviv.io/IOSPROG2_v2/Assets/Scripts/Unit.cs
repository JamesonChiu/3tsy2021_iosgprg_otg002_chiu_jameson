﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
    public string name;
    // public int HP;
    public int moveSpeed;
    public int ammo;
    //public int firerate;
    public float reloadSpeed;
    // public HP_Bar healthBar;
    // public Weapon Gun;
    
   /* [System.Serializable]
    /#public class Ammo
    {
        public string ammoType;
       // public int amount;
    }
    public Ammo[] inventory = new Ammo[3];
    /*public Weapons Primary;
    public Weapons Secondary;
    public Weapons CurrentEquip;*/

    public GameObject[] Slots;
    public int CurrentEquip;

    

    public void init(string nName, int nHealth, int nMoveSpeed)
    {
        this.name = nName;
        //this.HP = nHealth;
        this.moveSpeed = nMoveSpeed;

        if(this.GetComponent<Health>())
        {
            this.GetComponent<Health>().Init(nHealth);
        }
    }

    public void Firing()
    {
       
        this.Slots[CurrentEquip].GetComponent<Weapon>().Shoot();
        
    }

    public void Reloading()
    {
        Debug.Log("Reload");
        Pistol.Instance.currentAmmo = Pistol.Instance.clip_size;
        Pistol.Instance.max_ammo -= Pistol.Instance.clip_size;
    }

    public void TakeDamage(int damage)
    {
        this.GetComponent<Health>().Damaged(damage);
       
    }

    public void Death()
    {

    }
}


/* inventory[0].ammoType = "pistolAmmo";
     //  inventory[0].amount = 0;
       inventory[1].ammoType = "shotgunAmmo";
      // inventory[1].amount = 0;
       inventory[2].ammoType = "ARAmmo";
     //  inventory[2].amount = 0;*/
