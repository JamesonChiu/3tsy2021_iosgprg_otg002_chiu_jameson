﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(HP_Bar))]

public class Player : Unit
{
    public Joystick controller;
    public Joystick aim;

    public Rigidbody2D rb;

    float H_Movement = 0f;
    float V_Movement = 0f;

    public GameObject[] guns = new GameObject[3];

    Vector2 Movement;
    Vector3 Directionals;
    Vector3 Joystick_Directionals;

    //[SerializeField] bool isFiring;
    //[SerializeField] bool isReloading;

    public HP_Bar healthBar;

    public static Player Instance;
    private void Start()
    {
        this.GetComponent<Player>().init("Player", 100, 500);
        // this.gameObject.GetComponent<Health>().dmg += healthBar.setHP;
        // this.gameObject.GetComponent<Health>().act += 

        CurrentEquip = 0;

        

    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        this.gameObject.GetComponent<Health>().act += healthBar.setMaxHP;
        this.gameObject.GetComponent<Health>().dmg += healthBar.setHP;

    }

    void Update()
    {


        H_Movement = controller.Horizontal * moveSpeed;
        V_Movement = controller.Vertical * moveSpeed;

        Movement = new Vector2(H_Movement, V_Movement);

        this.rb.freezeRotation = true;

        Joystick_Directionals = new Vector3(aim.Horizontal, aim.Vertical, 0); //if joystick moves thru x and y
        if (Joystick_Directionals != new Vector3(0, 0, 0))
        {
            transform.eulerAngles = new Vector3(0, 0, (Mathf.Atan2(aim.Vertical, aim.Horizontal) * Mathf.Rad2Deg) - 90f);
            this.rb.freezeRotation = false;

            // Fire();

            if (Slots[CurrentEquip].activeSelf != false)
            {
                Firing();
            }

        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            TakeDamage(10);
            Debug.Log("Damaged");
        }

        if (this.Slots[CurrentEquip] != null && Slots[CurrentEquip].GetComponent<Weapon>().currentAmmo <= 0)
        {
            Debug.Log("Reload");
            Slots[CurrentEquip].GetComponent<Weapon>().Reload();
        }

       /* if (this.Slots[CurrentEquip] != null && Slots[CurrentEquip].GetComponent<Weapon>().currentAmmo <= 0 && Slots[CurrentEquip].GetComponent<Weapon>().max_ammo <= 0)
        {
            Debug.Log("Empty");
            if (guns[1].activeSelf == true)
            {
                guns[1].SetActive(false);
            }
        }*/

        if (this.GetComponent<Health>().currentHP <= 0)
        {
            Destroy(this.gameObject);
        }



    }

    private void FixedUpdate()
    {
        rb.velocity = Movement * Time.fixedDeltaTime;
      
    }

    

    private void OnTriggerEnter2D(Collider2D collision) // PLAYER PICKUP
    {
        if(collision.gameObject.CompareTag("Shotgun"))
        {
            if (guns[0].activeSelf == true)
            {
                guns[1].SetActive(false); // set sprite false
            }
            else
            {
                guns[1].SetActive(true); // set sprite true
            }

            if (guns[2].activeSelf == true)
            {
                guns[2].SetActive(false);
            }

            Slots[0] = guns[1]; // set sprite as equip weapon and slot
             CurrentEquip = 0;
           
   
            if (Slots[0].CompareTag(collision.gameObject.tag) || CurrentEquip == 1) // PRIMARY
            {
                Slots[0].GetComponent<Weapon>().max_ammo += Slots[0].GetComponent<Weapon>().clip_size;
              
            }

            Destroy(collision.gameObject);
            WeaponSpawner.Instance.RemoveToList_Weapon(collision.gameObject);
        }

        else if (collision.gameObject.CompareTag("AR"))
        {
            if (guns[0].activeSelf == true)
            {
                guns[2].SetActive(false); // set sprite false
            }
            else
            {
                guns[2].SetActive(true); // set sprite true
            }

            if (guns[1].activeSelf == true)
            {
                guns[1].SetActive(false);
            }

            Slots[0] = guns[2]; // set sprite as equip weapon and slot
            CurrentEquip = 0;

            if (Slots[0].CompareTag(collision.gameObject.tag) || CurrentEquip == 1) // PRIMARY
            {
                Slots[0].GetComponent<Weapon>().max_ammo += Slots[0].GetComponent<Weapon>().clip_size;

            }

            Destroy(collision.gameObject);
            WeaponSpawner.Instance.RemoveToList_Weapon(collision.gameObject);
        }

        else if(collision.gameObject.CompareTag("Pistol"))
        {        
            if(guns[1].activeSelf == true || guns[2].activeSelf == true)
            {
                guns[0].SetActive(false); // set sprite false
            }
            else
            {
                guns[0].SetActive(true); // set sprite true
            }

            Slots[1] = guns[0]; // set sprite as equip weapon and slot
            CurrentEquip = 1;

            if (Slots[1].CompareTag(collision.gameObject.tag) || CurrentEquip == 0) // SECONDARY
            {
                Slots[1].GetComponent<Weapon>().max_ammo += Slots[1].GetComponent<Weapon>().clip_size;

            }

            Destroy(collision.gameObject);
            WeaponSpawner.Instance.RemoveToList_Weapon(collision.gameObject);
        }

        else if (collision.gameObject.CompareTag("Pistol_Bullet"))
        {
            if (Slots[1].CompareTag("Pistol") || CurrentEquip == 0) // SECONDARY
            {
                Debug.Log("Pistol_Ammo");
                Slots[1].GetComponent<Weapon>().max_ammo += 14;

            }

            Destroy(collision.gameObject);
            WeaponSpawner.Instance.RemoveToList_Ammo(collision.gameObject);
        }
        else if (collision.gameObject.CompareTag("Shotgun_Bullet"))
        {
            if (Slots[0].CompareTag("Shotgun") || CurrentEquip == 1) // PRIMARY
            {
                Slots[0].GetComponent<Weapon>().max_ammo += 4;

            }
            Destroy(collision.gameObject);
            WeaponSpawner.Instance.RemoveToList_Ammo(collision.gameObject);
        }
        else if (collision.gameObject.CompareTag("AR_Bullet"))
        {
            if (Slots[0].CompareTag("AR") || CurrentEquip == 1) // PRIMARY
            {
                Slots[0].GetComponent<Weapon>().max_ammo += 60;

            }
            Destroy(collision.gameObject);
            WeaponSpawner.Instance.RemoveToList_Ammo(collision.gameObject);
        }


    }
}









/* Directionals = new Vector3(0,0,Mathf.Atan(aim.Vertical/aim.Horizontal) * 360);
         transform.rotation = Quaternion.Euler(Directionals);*/

/*Directionals = new Vector3(transform.position.x + aim.Horizontal, transform.position.y + aim.Vertical, Mathf.Atan2(aim.Horizontal, aim.Vertical) * Mathf.Rad2Deg);
// Vector3 LookPos = transform.position + Directionals;
this.transform.rotation = Quaternion.LookRotation(Directionals);
           // this.transform.up = Joystick_Directionals;*/


/*Debug.Log(collision.gameObject.name);
       pistol_equip = collision.gameObject;
       collision.gameObject.transform.SetParent(this.gameObject.transform);
       collision.transform.position = pistol_equip.transform.position;
      */


/* if (collision.gameObject.CompareTag("Pistol"))
    {
        //equip pistol
        Debug.Log("Pistol");
        //Secondary = Weapons.Pistol;
        //CurrentEquip = Weapons.Pistol;
        pistol_equip.SetActive(true);
        Destroy(collision.gameObject);
        WeaponSpawner.Instance.RemoveToList_Weapon(collision.gameObject);

        if(Secondary == Weapons.Pistol)
        {
            Pistol.Instance.max_ammo += 7;
        }

    }

    else if (collision.gameObject.CompareTag("Shotgun"))
    {
        //equip Shotgun
        Debug.Log("Shotgun");
        Primary = Weapons.Shotgun;
        CurrentEquip = Weapons.Shotgun;
        shotgun_equip.SetActive(true);
        Destroy(collision.gameObject);
        WeaponSpawner.Instance.RemoveToList_Weapon(collision.gameObject);

        if(pistol_equip.activeSelf == true)
        {
            pistol_equip.SetActive(false);
        }
    }

    else if (collision.gameObject.CompareTag("AR"))
    {
        //equip AR
        Debug.Log("AR");
        Primary = Weapons.AR;
        Destroy(collision.gameObject);
        WeaponSpawner.Instance.RemoveToList_Weapon(collision.gameObject);
    }

    else if (collision.gameObject.CompareTag("Pistol_Bullet"))
    {
        //Add Pistol Ammo
        Debug.Log("Pistol_Ammo");
        if(Secondary == Weapons.Pistol)
        {
            Pistol.Instance.max_ammo += 10;


        }
        else if (Secondary == Weapons.None)
        {
            Pistol_Ammo_Inventory += 10;
        }


        Destroy(collision.gameObject);

    }
    else if (collision.gameObject.CompareTag("Shotgun_Ammo"))
    {
        //Add Shotgun_Ammo
        Destroy(collision);
    }
    else if (collision.gameObject.CompareTag("AR_Ammo"))
    {
        //Add AR_Ammo
        Destroy(collision);
    }*/

/*if (guns[0].activeSelf == true)
        {
             Debug.Log("Pistol");
            Slots[1].SetActive(false);
        }*/


/*for (int checkWeaponSprite = 0; checkWeaponSprite < guns.Length; checkWeaponSprite++) // Looking for weapon sprites
{
    if(guns[checkWeaponSprite].CompareTag(collision.gameObject.tag) == true) // if current weapon mathces the tag of the object that was hit
    {
        guns[checkWeaponSprite].SetActive(true); // set sprite true
        Slots[CurrentEquip] = guns[checkWeaponSprite]; // set sprite as equip weapon and slot

    }
}*/

/* Primary = Weapons.None;
     Secondary = Weapons.None;
     CurrentEquip = Weapons.None;*/

/*if(Slots[CurrentEquip] != null)
   {
       for (int i = 0; i < inventory.Length; i++)
       {
           if (inventory[i].ammoType == Slots[CurrentEquip].GetComponent<Weapon>().bulletType.ToString())
           {
               Slots[CurrentEquip].GetComponent<Weapon>().max_ammo += inventory[i].amount;
               inventory[i].amount = 0;
               //Pistol_Ammo_Inventory = 0;
           }
       }
   }*/

//isFiring = false;
//isReloading = false;
// healthBar.setMaxHP(HP);