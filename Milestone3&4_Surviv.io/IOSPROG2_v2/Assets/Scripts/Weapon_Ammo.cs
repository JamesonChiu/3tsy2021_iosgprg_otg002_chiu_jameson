﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon_Ammo : MonoBehaviour
{
    public string ammo_Name;
    public int bullet_Speed;
    public float timer;


    public void Ammo_Init(string ammoName, int bulletSpeed)
    {
        this.ammo_Name = ammoName;
        this.bullet_Speed = bulletSpeed;
    }


}
