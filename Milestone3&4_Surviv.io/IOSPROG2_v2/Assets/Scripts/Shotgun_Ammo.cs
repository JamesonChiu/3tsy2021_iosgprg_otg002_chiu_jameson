﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotgun_Ammo : Weapon_Ammo
{
    private void Awake()
    {
        this.GetComponent<Shotgun_Ammo>().Ammo_Init("Shotgun_Ammo", 20);
    }
    private void Update()
    {
        if (timer >= 100)
        {
            Destroy(this.gameObject);
            timer = 0;
        }
        timer++;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            //this.gameObject.SetActive(false);
            Destroy(this.gameObject);
            collision.gameObject.GetComponent<Player>().TakeDamage(2);
        }

        else if (collision.gameObject.CompareTag("Enemy"))
        {
            // this.gameObject.SetActive(false);
            Destroy(this.gameObject);
            collision.gameObject.GetComponent<Health>().currentHP -= 2;
        }

        else if (collision.gameObject.CompareTag("Obstacle"))
        {
            Destroy(this.gameObject);
        }


    }
}
