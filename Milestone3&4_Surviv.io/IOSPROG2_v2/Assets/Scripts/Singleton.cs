﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton : MonoBehaviour
{
    public static Singleton Instance;

    public GameObject player;
    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }
}
