﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Attack : Ai_Control
{
   // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
    }

  //  OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if(AI_Entity.GetComponent<Enemy>().Slots[AI_Entity.GetComponent<Enemy>().CurrentEquip].activeSelf != false)
        {
            Debug.Log("Enemy Attack");
            AI_Entity.GetComponent<Enemy>().Firing();
        }
    }

   // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
    }


}
