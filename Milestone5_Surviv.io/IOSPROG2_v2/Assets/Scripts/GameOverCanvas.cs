﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverCanvas : MenuMainCanvas
{

    public void Retry()
    {
        GameMgr.Instance.ResetGame();
        GameMgr.Instance.OnGameLoaded();
       
    }

    public void QuitGame()
    {
        Application.Quit();
    }

}
