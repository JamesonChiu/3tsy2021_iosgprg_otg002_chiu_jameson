﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotgun : Weapon
{
    // Start is called before the first frame update

    public GameObject ShotgunAmmo;
    public Transform Shotgun_Nuzzle;
    public Weapons weaponType;
    [SerializeField] float ShotTime;

    public Transform shotgunTarget;
    void Start()
    {
        this.GetComponent<Shotgun>().Weapon_Init("Shotgun", 2, 2, 12, 2);
        ShotTime = 0f;

    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void Shoot()
    {
        if (Time.time >= ShotTime)
        {
            Quaternion DefaultRotation = Shotgun_Nuzzle.rotation;

            for(int pellet = 0; pellet < 8; pellet++)
            {
                 GameObject newBullet = Instantiate(ShotgunAmmo);
                 newBullet.transform.position = Shotgun_Nuzzle.position;

                float coneSpread = Random.Range(-0.2f, 0.2f);

                Shotgun_Nuzzle.rotation = new Quaternion(Shotgun_Nuzzle.rotation.x, Shotgun_Nuzzle.rotation.y, Shotgun_Nuzzle.rotation.z + coneSpread, Shotgun_Nuzzle.rotation.w);
                int speed = Random.Range(-2, 2);

                newBullet.GetComponent<Rigidbody2D>().AddForce(Shotgun_Nuzzle.up * (newBullet.GetComponent<Shotgun_Ammo>().bullet_Speed + speed), ForceMode2D.Impulse);
                Shotgun_Nuzzle.rotation = DefaultRotation;
            }
            ShotTime = Time.time + fireRate;
            currentAmmo--;
        }
    }
    public void Shotgun_Reload()
    {

        Debug.Log("Reload");
       
        currentAmmo = clip_size;
        max_ammo -= clip_size;
        
    }

}


/* int offset = Random.Range(-20, 20);
                GameObject newBullet = Instantiate(ShotgunAmmo, Shotgun_Nuzzle.position, Quaternion.LookRotation(Shotgun_Nuzzle.forward) * Quaternion.Euler(new Vector3(0,0,offset)));
                newBullet.GetComponent<Rigidbody2D>().velocity = Shotgun_Nuzzle.up * Time.deltaTime * newBullet.GetComponent<Shotgun_Ammo>().bullet_Speed; 
                ShotTime = Time.time + fireRate;*/
//Vector3 OffSet = new Vector3(Random.Range(-3, 3), Random.Range(0, 4), Shotgun_Nuzzle.transform.position.z);