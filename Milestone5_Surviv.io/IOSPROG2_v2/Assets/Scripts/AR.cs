﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AR : Weapon
{
    public GameObject RifleAmmo;
    public Transform AR_Nuzzle;
    public Weapons weaponType;
    [SerializeField] float ShotTime;

    public Transform arTarget;
    void Start()
    {
        this.GetComponent<AR>().Weapon_Init("AR", 30, 30, 90, 0.1f);
        ShotTime = 0f;

    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void Shoot()
    {
        if (Time.time >= ShotTime)
        {
            Quaternion DefaultRotation = AR_Nuzzle.rotation;
    
            GameObject newBullet = Instantiate(RifleAmmo);
            newBullet.transform.position = AR_Nuzzle.position;

            float spread = Random.Range(-0.1f, 0.1f);

            AR_Nuzzle.rotation = new Quaternion(AR_Nuzzle.rotation.x, AR_Nuzzle.rotation.y, AR_Nuzzle.rotation.z + spread, AR_Nuzzle.rotation.w);
            int speed = Random.Range(-2, 2);

            newBullet.GetComponent<Rigidbody2D>().AddForce(AR_Nuzzle.up * (newBullet.GetComponent<AR_Ammo>().bullet_Speed + speed), ForceMode2D.Impulse);
            AR_Nuzzle.rotation = DefaultRotation;
            
            ShotTime = Time.time + fireRate;
            currentAmmo--;          
        }
    }
    public void AR_Reload()
    {
      
            Debug.Log("Reload");

            currentAmmo = clip_size;
            max_ammo -= clip_size;
            
        
    }

}

