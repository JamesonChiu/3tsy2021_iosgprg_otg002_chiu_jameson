﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Health : MonoBehaviour
{
    public int currentHP;
    [SerializeField] int MaxHP;
    public int maxHP { get { return MaxHP; } }


    //public HP_Bar healthBar;

    public Action <int> act;
    public Action<int> dmg;

    public void Init(int maxHealth)
    {
        this.MaxHP = maxHealth;
        currentHP = MaxHP;
        //healthBar.setMaxHP(MaxHP);\
        act.Invoke(MaxHP);
    }

   public void Damaged(int damage)
   {
        currentHP -= damage;
        //healthBar.setHP(currentHP);
        dmg.Invoke(currentHP);
    }

}
