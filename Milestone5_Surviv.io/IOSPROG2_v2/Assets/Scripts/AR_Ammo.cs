﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AR_Ammo : Weapon_Ammo
{
    private void Awake()
    {
        this.GetComponent<AR_Ammo>().Ammo_Init("Ar_Ammo", 35);
    }
    private void Update()
    {
        if (timer >= 150)
        {
            Destroy(this.gameObject);
            timer = 0;
        }
        timer++;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            //this.gameObject.SetActive(false);
            Destroy(this.gameObject);
            collision.gameObject.GetComponent<Player>().TakeDamage(3);
        }

        else if (collision.gameObject.CompareTag("Enemy"))
        {
            // this.gameObject.SetActive(false);
            Destroy(this.gameObject);
            collision.gameObject.GetComponent<Health>().currentHP -= 3;
        }

        else if (collision.gameObject.CompareTag("Obstacle"))
        {
            Destroy(this.gameObject);
        }


    }
}
