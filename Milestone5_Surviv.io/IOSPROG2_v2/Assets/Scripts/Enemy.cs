﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : Unit
{
    public float waitTime;
    public float startWaitTime;

    public float stoppingDistance;

    public Animator animator;

    Vector2 WanderTarget;

    public Transform playerTarget;

    public Transform pistolTarget;
    public Transform shotgunTarget;
    public Transform ARTarget;

    public Transform LootTarget;

    public Vector2 moveSpots;
    public Collider2D[] colliders;

    public int index;
   // public int followSpot;

   


    public GameObject[] guns = new GameObject[3];


    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Enemy>().init("Enemy", 100, 10);
        waitTime = startWaitTime;

        moveSpots = new Vector2(UnityEngine.Random.Range(-100f, 100f), UnityEngine.Random.Range(-55f, 55f));

        playerTarget = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();

        /*pistolTarget = GameObject.FindGameObjectWithTag("Pistol").GetComponent<Transform>();
        shotgunTarget = GameObject.FindGameObjectWithTag("Shotgun").GetComponent<Transform>();
        arTarget = GameObject.FindGameObjectWithTag("AR").GetComponent<Transform>();*/
    }

    public void PickUp()
    {

    }
        
    public void AI_Attack()
    {
        if (Slots[CurrentEquip].activeSelf != false)
        {
            Firing();
        }
    }

    public void Update()
    {
        animator.SetFloat("Distance", Vector3.Distance(transform.position, playerTarget.position));

        EnemyLoot();

        if(this.GetComponent<Health>().currentHP <= 0)
        {
            if (this.GetComponent<Enemy>().Slots[0] != null)
            {
                this.GetComponent<Enemy>().Slots[0] = null;
                if (guns[1].activeSelf == true)
                {
                    guns[1].SetActive(false); 
                }
                else if (guns[2].activeSelf == true)
                {
                    guns[2].SetActive(false);
                }
            }
            this.gameObject.SetActive(false);
            Vector3 newPos = new Vector3(UnityEngine.Random.Range(-100f, 100f), UnityEngine.Random.Range(-55f, 55f), 0);
            this.transform.position = newPos;
            this.gameObject.SetActive(true);
            this.GetComponent<Enemy>().init("Enemy", 100, 10);
            
            
            //Score +1;
            // Destroy(this.gameObject);
        }

    }

    public void EnemyLoot()
    {
        colliders = Physics2D.OverlapCircleAll(this.transform.position, 10.0f);


        if (LootTarget != null)
        {
            animator.SetFloat("LootDistance", Vector3.Distance(transform.position, LootTarget.position));

        }

        if (colliders.Length > 1)
        {

            if (LootTarget == null)
            {
                index = UnityEngine.Random.Range(1, colliders.Length);
            }

            if ((/*!colliders[index].gameObject.CompareTag("Pistol") && LootTarget == null ||*/ !colliders[index].gameObject.CompareTag("Shotgun") && LootTarget == null || !colliders[index].gameObject.CompareTag("AR") && LootTarget == null))
            {
                index = UnityEngine.Random.Range(1, colliders.Length);


                if (/*colliders[index].gameObject.CompareTag("Pistol") && LootTarget == null ||*/ colliders[index].gameObject.CompareTag("Shotgun") && LootTarget == null || colliders[index].gameObject.CompareTag("AR") && LootTarget == null)
                {
                    LootTarget = colliders[index].gameObject.transform.root.gameObject.transform;

                }
            }
            Debug.Log(colliders[index].gameObject.transform.root.gameObject.transform);

        }
    }

    private void OnTriggerEnter2D(Collider2D collision) // ENEMY PICKUP
    {
        if (collision.gameObject.CompareTag("Shotgun"))
        {
            if (guns[0].activeSelf == true)
            {
                guns[1].SetActive(false); // set sprite true
            }
            else
            {
                guns[1].SetActive(true); // set sprite true
            }

            if (guns[2].activeSelf == true)
            {
                guns[2].SetActive(false);
            }

            Slots[0] = guns[1]; // set sprite as equip weapon and slot
            CurrentEquip = 0;


            if (Slots[0].CompareTag(collision.gameObject.tag) || CurrentEquip == 1) // PRIMARY
            {
                Slots[0].GetComponent<Weapon>().max_ammo += Slots[0].GetComponent<Weapon>().clip_size;

            }

            Destroy(collision.gameObject);
            WeaponSpawner.Instance.RemoveToList_Weapon(collision.gameObject);


            animator.SetFloat("LootDistance", 999);
            LootTarget = null;

        }

        else if (collision.gameObject.CompareTag("AR"))
        {
            if (guns[0].activeSelf == true)
            {
                guns[2].SetActive(false); // set sprite true
            }
            else
            {
                guns[2].SetActive(true); // set sprite true
            }

            if (guns[1].activeSelf == true)
            {
                guns[1].SetActive(false);
            }

            Slots[0] = guns[2]; // set sprite as equip weapon and slot
            CurrentEquip = 0;

            if (Slots[0].CompareTag(collision.gameObject.tag) || CurrentEquip == 1) // PRIMARY
            {
                Slots[0].GetComponent<Weapon>().max_ammo += Slots[0].GetComponent<Weapon>().clip_size;

            }

            Destroy(collision.gameObject);
            WeaponSpawner.Instance.RemoveToList_Weapon(collision.gameObject);

            
            animator.SetFloat("LootDistance", 999);
            LootTarget = null;

        }

       /* else if (collision.gameObject.CompareTag("Pistol"))
        {
            if (guns[1].activeSelf == true || guns[2].activeSelf == true)
            {
                guns[0].SetActive(false); // set sprite true
            }
            else
            {
                guns[0].SetActive(true); // set sprite true
            }

            Slots[1] = guns[0]; // set sprite as equip weapon and slot
            CurrentEquip = 1;

            if (Slots[1].CompareTag(collision.gameObject.tag) || CurrentEquip == 0) // SECONDARY
            {
                Slots[1].GetComponent<Weapon>().max_ammo += Slots[1].GetComponent<Weapon>().clip_size;

            }

            Destroy(collision.gameObject);
            WeaponSpawner.Instance.RemoveToList_Weapon(collision.gameObject);

            
            animator.SetFloat("LootDistance", 999);
            LootTarget = null;

        } */

    }

}

/*void Hide()
   {
       float distance = Mathf.Infinity;
       Vector3 chosenSpot = Vector3.zero;

       int hidingSpotCount = World.Instance.GetHindingSpots().Length;
       for (int i = 0; i < hidingSpotCount; i++)
       {
           Vector3 hideDirection = World.Instance.GetHindingSpots()[i].transform.position - target.transform.position;
           Vector3 hidePosition = World.Instance.GetHindingSpots()[i].transform.position + hideDirection.normalized * 5;

           float spotDistance = Vector3.Distance(this.transform.position, hidePosition);
           if (spotDistance < distance)
           {
               chosenSpot = hidePosition;
               distance = spotDistance;
           }
       }

       Seek(chosenSpot);
   }*/

/*void CleverHide()
{
    float distance = Mathf.Infinity;
    Vector3 chosenSpot = Vector3.zero;
    Vector3 chosenDir = Vector3.zero;
    GameObject chosenGameObject = World.Instance.GetHindingSpots()[0];

    int hidingSpotCount = World.Instance.GetHindingSpots().Length;
    for (int i = 0; i < hidingSpotCount; i++)
    {
        Vector3 hideDirection = World.Instance.GetHindingSpots()[i].transform.position - target.transform.position;
        Vector3 hidePosition = World.Instance.GetHindingSpots()[i].transform.position + hideDirection.normalized * 5;

        float spotDistance = Vector3.Distance(this.transform.position, hidePosition);
        if (spotDistance < distance)
        {
            chosenSpot = hidePosition;
            chosenDir = hideDirection;
            chosenGameObject = World.Instance.GetHindingSpots()[i];
            distance = spotDistance;
        }
    }

    Collider hideCol = chosenGameObject.GetComponent<Collider>();
    Ray back = new Ray(chosenSpot, -chosenDir.normalized);
    RaycastHit info;
    float rayDistance = 100f;
    hideCol.Raycast(back, out info, rayDistance);


    Seek(info.point + chosenDir.normalized * 5);
}*/

/*void Flee(Vector2 location)
{
Vector2 fleeDirection = location - this.transform.position;
agent.SetDestination(this.transform.position - fleeDirection);
}*/

/* void Seek(Vector2 location)
{
    agent.SetDestination(location);
}

void Pursue()
{
    Vector2 targetDirection = target.transform.position - this.transform.position;

    float lookAhead = targetDirection.magnitude / agent.speed;

    Seek(target.transform.position + target.transform.forward * lookAhead);
}


void Wander()
{
    float wanderRadius = 20;
    float wanderDistance = 10;
    float wanderJitter = 1;

    WanderTarget += new Vector2(Random.Range(-1.0f, 1.0f) * wanderJitter, Random.Range(-1.0f, 1.0f * wanderJitter));
    WanderTarget.Normalize();
    WanderTarget *= wanderRadius;

    Vector2 targetLocal = WanderTarget + new Vector2(0, wanderDistance);
    Vector2 targetWorld = this.gameObject.transform.InverseTransformVector(targetLocal);

    Seek(targetWorld);


     transform.position = Vector2.MoveTowards(transform.position, Destination_Pos.position, moveSpeed * Time.deltaTime);

        if(Vector2.Distance(transform.position, Destination_Pos.position) < 0.2f)
        {
            if(waitTime <-0)
            {
                Destination_Pos.position = new Vector2(Random.Range(-100f, 100f), Random.Range(-100f, 100f));
                waitTime = startWaitTime;
            }
            else
            {
                waitTime -= Time.deltaTime;
            }
        }
}

bool CanSeeTarget()
{
    RaycastHit raycastInfo;
    Vector2 rayToTarget = target.transform.position - this.transform.position;
    if (Physics.Raycast(this.transform.position, rayToTarget, out raycastInfo))
    {
        return raycastInfo.transform.gameObject.tag == "Player";
    }

    return false;
}*/
/*public void Wander()
   {
       transform.position = Vector2.MoveTowards(transform.position, moveSpots[followSpot].position, moveSpeed * Time.deltaTime);
       transform.up = moveSpots[followSpot].position - transform.position;

       if(Vector2.Distance(transform.position, moveSpots[followSpot].position) < 0.2f)
       {
           if(waitTime <= 0)
           {
               moveSpots[followSpot].position = new Vector2(Random.Range(-100f, 100f), Random.Range(-55f, 55f));
               waitTime = startWaitTime;
           }
           else
           {
               waitTime -= Time.deltaTime;
           }
       }
   }*/

/* public void Follow()
 {
     if(Vector2.Distance(transform.position, playerTarget.position) > stoppingDistance)
     {
         transform.position = Vector2.MoveTowards(transform.position, playerTarget.position, moveSpeed * Time.deltaTime);
         transform.up = playerTarget.position - transform.position;
     }
 }

 public void Loot()
 {
     if (Vector2.Distance(pistolTarget.position, transform.position) < 10)
     {
         transform.position = Vector2.MoveTowards(transform.position, pistolTarget.position, moveSpeed * Time.deltaTime);
         transform.up = pistolTarget.position - transform.position;
     }
     else if (Vector2.Distance(shotgunTarget.position, transform.position) < 10)
     {
         transform.position = Vector2.MoveTowards(transform.position, shotgunTarget.position, moveSpeed * Time.deltaTime);
         transform.up = shotgunTarget.position - transform.position;
     }
     else if(Vector2.Distance(arTarget.position, transform.position) < 10)
     {
         transform.position = Vector2.MoveTowards(transform.position, arTarget.position, moveSpeed * Time.deltaTime);
         transform.up = shotgunTarget.position - transform.position;
     }

 }*/

     /*else if (collision.gameObject.CompareTag("Pistol_Bullet"))
        {
            if (Slots[1].CompareTag("Pistol") || CurrentEquip == 0) // SECONDARY
            {
                Debug.Log("Pistol_Ammo");
                Slots[1].GetComponent<Weapon>().max_ammo += 14;

            }

            Destroy(collision.gameObject);
WeaponSpawner.Instance.RemoveToList_Weapon(collision.gameObject);
        }
        else if (collision.gameObject.CompareTag("Shotgun_Bullet"))
        {
            if (Slots[0].CompareTag("Shotgun") || CurrentEquip == 1) // PRIMARY
            {
                Slots[0].GetComponent<Weapon>().max_ammo += 4;

            }
            Destroy(collision.gameObject);
WeaponSpawner.Instance.RemoveToList_Weapon(collision.gameObject);
        }
        else if (collision.gameObject.CompareTag("AR_Bullet"))
        {
            if (Slots[0].CompareTag("AR") || CurrentEquip == 1) // PRIMARY
            {
                Slots[0].GetComponent<Weapon>().max_ammo += 60;

            }
            Destroy(collision.gameObject);
WeaponSpawner.Instance.RemoveToList_Weapon(collision.gameObject);
        }*/
