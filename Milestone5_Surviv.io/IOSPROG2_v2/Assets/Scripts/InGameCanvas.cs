﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameCanvas : MenuMainCanvas
{

    public Joystick controller;
    public Joystick aim;

    private Player player;

    public Slider HPslide;


    public Text Current_Ammo;
    public Text max_Ammo;

    //private HP_Bar healthBar;

    public void Update()
    {
       if(player == null)
       {
           player = GameMgr.Instance.player;
       }

        HPslide.value = player.GetComponent<Health>().currentHP;

        player.H_Movement = controller.Horizontal * player.moveSpeed;
        player.V_Movement = controller.Vertical * player.moveSpeed;

        player.Movement = new Vector2(player.H_Movement, player.V_Movement);

        player.rb.freezeRotation = true;

        player.Joystick_Directionals = new Vector3(aim.Horizontal, aim.Vertical, 0); //if joystick moves thru x and y
        if (player.Joystick_Directionals != new Vector3(0, 0, 0))
        {
            player.transform.eulerAngles = new Vector3(0, 0, (Mathf.Atan2(aim.Vertical, aim.Horizontal) * Mathf.Rad2Deg) - 90f);
            player.rb.freezeRotation = false;

            // Fire();

            if (player.Slots[player.CurrentEquip].activeSelf != false) // Firing
            {
                player.Firing();
            }

        }

        if (player.Slots[player.CurrentEquip] != null)
        {
            Current_Ammo.text = player.Slots[player.CurrentEquip].GetComponent<Weapon>().currentAmmo.ToString();
        }

        if (player.Slots[player.CurrentEquip] != null)
        {
            max_Ammo.text = player.Slots[player.CurrentEquip].GetComponent<Weapon>().max_ammo.ToString();
        }

        if(player.GetComponent<Health>().currentHP <= 0)
        {
            MenuMgr.Instance.ShowCanvas(MenuType.GameOver);
        }
    }
}

/* Player.Instance.H_Movement = controller.Horizontal * Player.Instance.moveSpeed;
        Player.Instance.V_Movement = controller.Vertical * Player.Instance.moveSpeed;

        Player.Instance.Movement = new Vector2(Player.Instance.H_Movement, Player.Instance.V_Movement);

        Player.Instance.rb.freezeRotation = true;

        Player.Instance.Joystick_Directionals = new Vector3(aim.Horizontal, aim.Vertical, 0); //if joystick moves thru x and y
        if (Player.Instance.Joystick_Directionals != new Vector3(0, 0, 0))
        {
            transform.eulerAngles = new Vector3(0, 0, (Mathf.Atan2(aim.Vertical, aim.Horizontal) * Mathf.Rad2Deg) - 90f);
            Player.Instance.rb.freezeRotation = false;

            // Fire();

            if (Player.Instance.Slots[Player.Instance.CurrentEquip].activeSelf != false) // Firing
            {
                Player.Instance.Firing();
            }

        }
*/
