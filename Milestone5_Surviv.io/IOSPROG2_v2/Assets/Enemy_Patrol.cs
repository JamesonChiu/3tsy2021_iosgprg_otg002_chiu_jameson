﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Patrol : Ai_Control
{
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
    }

    //OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        AI_Entity.GetComponent<Enemy>().moveSpeed = 10;
        Debug.Log(AI_Entity.GetComponent<Enemy>().moveSpeed + "MoveSpeed");

        AI_Entity.transform.position = Vector2.MoveTowards(AI_Entity.transform.position, AI_Entity.GetComponent<Enemy>().moveSpots, AI_Entity.GetComponent<Enemy>().moveSpeed * Time.deltaTime);
        AI_Entity.transform.up = AI_Entity.GetComponent<Enemy>().moveSpots - (Vector2)AI_Entity.transform.position;

      
        if (Vector2.Distance(AI_Entity.transform.position, AI_Entity.GetComponent<Enemy>().moveSpots) < 0.2f)
        {
            if (AI_Entity.GetComponent<Enemy>().waitTime <= 0)
            {
                AI_Entity.GetComponent<Enemy>().moveSpots = new Vector2(Random.Range(-100f, 100f), Random.Range(-55f, 55f));
                AI_Entity.GetComponent<Enemy>().waitTime = AI_Entity.GetComponent<Enemy>().startWaitTime;
            }
            else
            {
                AI_Entity.GetComponent<Enemy>().waitTime -= Time.deltaTime;
            }
        }
    }

   // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateExit(animator, stateInfo, layerIndex);
    }

}
