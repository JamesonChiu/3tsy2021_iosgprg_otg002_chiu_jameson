﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{ private float speed = 5f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position += this.transform.up*speed*Time.deltaTime;
        if(this.transform.position.y < -5f )
        {
            this.transform.position = new Vector3(this.transform.position.x, 5.5f, this.transform.position.z);
        }
    }
}
