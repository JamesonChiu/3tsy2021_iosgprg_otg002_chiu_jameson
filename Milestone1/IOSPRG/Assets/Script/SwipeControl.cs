﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeControl : MonoBehaviour
{
    private bool tap, swipeUp, swipeDown,swipeRight,swipeLeft;
    private bool isDraging = false;
    private bool moved;
    private Vector2 startTouch, swipeDelta;

    public GameObject attackUp;
    public GameObject attackDown;
    public GameObject attackLeft;
    public GameObject attackRight;

    public GameObject reverseAttackUP; // Down 
    public GameObject reverseAttackDOWN; // Up
    public GameObject reverseAttackLEFT; // RIGHT
    public GameObject reverseAttackRIGHT; // Left
    public Transform player;
    private Vector2 desiredPos;
    private Vector2 dragForce;
    
   void Start()
   {
       desiredPos = player.transform.position;
        dragForce =  new Vector2(50f,this.transform.position.y);
   }

    void Update()
    {
        ResetInput();

        //For Unity Editor
        if (Input.GetMouseButtonDown(0))
        {
            //tap = true;
            isDraging = true;
            moved = false;
            startTouch = Input.mousePosition;

        }
        else if (Input.GetMouseButtonUp(0))
        {
            isDraging = false;
            Reset();
        }
        

       //For mobile/bluestacks
        if (Input.touchCount > 0)
        {
            if (Input.touches[0].phase == TouchPhase.Began)
            {
                //tap = true;
                isDraging = true;
                moved = false;
                startTouch = Input.touches[0].position;
            }
            else if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled)
            {
                isDraging = false;
                Reset();
            }
        }

        swipeDelta = Vector2.zero;
        if (isDraging)
        {
            if (Input.touchCount > 0) // mobile
            {
                swipeDelta = Input.touches[0].position - startTouch;

            }
            else if (Input.GetMouseButton(0)) //unity editor
            {
                swipeDelta = (Vector2)Input.mousePosition - startTouch;
            }
        }

        if (swipeDelta.magnitude > 125)
        {
            moved = true;

            float x = swipeDelta.x;
            float y = swipeDelta.y;
            if(Mathf.Abs(x) > Mathf.Abs(y))
            {
               if (x<0)
               {
                   swipeLeft = true;
               } 
               else 
               {
                   swipeRight = true;
               }
            }
            else
            {
                if (y<0)
                {
                    swipeUp = true;
                }
                else
                {
                    swipeDown = true;
                }
            }

            Reset();
        }

       

        
        if (swipeUp)
        {
            Debug.Log("UpSwipe");
            attackUp.SetActive(false);
            reverseAttackDOWN.SetActive(false);
        }

        if (swipeDown)
        {
            Debug.Log("DownSwipe");
            attackDown.SetActive(false);
            reverseAttackUP.SetActive(false);
        }
        if(swipeLeft)
        {
            Debug.Log("LeftSwipe");
            attackLeft.SetActive(false);
            reverseAttackRIGHT.SetActive(false); 
        }
        if (swipeRight)
        {
             Debug.Log("RightSwipe");
            attackRight.SetActive(false);
            reverseAttackLEFT.SetActive(false);
        }

        if (tap)
        {
            Debug.Log("Tap");
            player.transform.Translate(dragForce*Time.deltaTime);
        }

    }


    void Reset()
    {
        if (!moved) 
        {
            tap = true;
        }
        startTouch = swipeDelta = Vector2.zero;
        isDraging = false;
    }

    void ResetInput()
    {
        tap = swipeDown = swipeUp = swipeLeft = swipeRight = false;
    }
    
}
