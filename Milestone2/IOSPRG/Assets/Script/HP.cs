﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HP : MonoBehaviour
{
    // Start is called before the first frame update
    public float Lives = 3;
    Text Health;
    void Start()
    {
       Health = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        Health.text = "Health: " + Lives;
    }

    public void Damage()
    {
        Lives -= 1;
    }

    public void ExtraHealth()
    {
        Lives += 1;
    }


}
