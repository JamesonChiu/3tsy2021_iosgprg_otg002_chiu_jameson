﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RollingEnemy : MonoBehaviour
{
     private float speed;
    private float timer = 0f;

    [SerializeField] SpriteRenderer RNG_Arrow;
    [SerializeField] Sprite[] Directional_Arrows;
    public direction D_Arrow; 

    bool isRolling = true;
    void Start()
    {
        StartCoroutine(EnemyRNG());
    }

    
    void Update()
    {
        speed = 3f;
        this.transform.position -= this.transform.up*speed*Time.deltaTime;
        timer++;
        if(timer >= 1000)
        {
            //Debug.Log("Erase");
            RemoveArrow();
        }
    }
     public void RemoveArrow()
    {
      Debug.Log(this.gameObject.activeSelf);
      this.gameObject.SetActive(false);
    }

    IEnumerator EnemyRNG()
    {
       
        while(isRolling)
        {
            RNG_Arrow.GetComponent<SpriteRenderer>().sprite = Directional_Arrows[Random.Range(0,Directional_Arrows.Length)];
            SetDirection();
            yield return new WaitForSeconds(5f);
            StartCoroutine(Stop());
            //yield return new WaitForSeconds(2f);
        }
    }

    IEnumerator Stop()
    {
        yield return new WaitForSeconds(1f);
        isRolling = false;
    }

    private void SetDirection()
    {
        if(RNG_Arrow.sprite.name == "Arrow_UP")
        {
            D_Arrow = direction.Up;
        }
        else if(RNG_Arrow.sprite.name == "Arrow_DOWN")
        {
            D_Arrow = direction.Down;
        }  
        else if(RNG_Arrow.sprite.name == "Arrow_LEFT")
        {
            D_Arrow = direction.Left;
        }  
        else if(RNG_Arrow.sprite.name == "Arrow_RIGHT")
        {
            D_Arrow = direction.Right;
        }
         /* else if(RNG_Arrow.sprite.name == "Arrow_rev_UP")
        {
            D_Arrow = direction.reverse_UP;
        }
        else if(RNG_Arrow.sprite.name == "Arrow_rev_DOWN")
        {
            D_Arrow = direction.reverse_Down;
        }
        else if(RNG_Arrow.sprite.name == "Arrow_rev_RIGHT")
        {
            D_Arrow = direction.reverse_Right;
        }
        else if(RNG_Arrow.sprite.name == "Arrow_rev_LEFT")
        {
            D_Arrow = direction.reverse_Left;
        }*/
    }

}
