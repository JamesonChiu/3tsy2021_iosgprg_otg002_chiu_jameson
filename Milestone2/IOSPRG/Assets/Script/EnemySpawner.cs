﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : Singleton<EnemySpawner>
{
   [SerializeField] List<Enemy> enemyList = new List<Enemy>();
   public GameObject[] E_arrows;

    float randX = 50f;
    float randY = 50f;

    void Start()
    {
        StartCoroutine(Spawn());

    }

    void Update()
    {

    }

   void SpanwEnemy()
   {
       float posX = Random.Range(-randX, randX);
       float posY = Random.Range(-randY, randY);
       int RNG = Random.Range(0,9);
      
      
       GameObject newArrows = (GameObject)Instantiate(E_arrows[RNG], new Vector3(this.transform.position.x,this.transform.position.y,0), Quaternion.identity);
       AddToList(newArrows.GetComponent<Enemy>());
   }

   void AddToList(Enemy arrows)
   {
       enemyList.Add(arrows);
   }

    IEnumerator Spawn()
    {
        
       float posX = Random.Range(-randX, randX);
       float posY = Random.Range(-randY, randY);
      
       while(true)
       {
            yield return new WaitForSeconds(3);
            int RNG = Random.Range(0,9);
            GameObject newArrows = (GameObject)Instantiate(E_arrows[RNG], new Vector3(this.transform.position.x,this.transform.position.y,0), Quaternion.identity);
            AddToList(newArrows.GetComponent<Enemy>());  
       }
    }
    
    public void RemoveArrow(GameObject removeObj)
    {
        enemyList.Remove(removeObj.GetComponent<Enemy>());
    }

    
    public void SwipeUP()
    {
        Enemy firstArrow = enemyList[0];

        if(firstArrow.GetComponent<Enemy>().directionalArrows == direction.Up || firstArrow.GetComponent<Enemy>().directionalArrows == direction.reverse_Down || firstArrow.GetComponent<RollingEnemy>().D_Arrow == direction.Up || firstArrow.GetComponent<RollingEnemy>().D_Arrow == direction.reverse_Down)
        {
            firstArrow.RemoveArrow();
            enemyList.RemoveAt(0);
            Score.score += 1;
        }
    }
     public void SwipeDown()
     {
         Enemy firstArrow = enemyList[0];

         if(firstArrow.GetComponent<Enemy>().directionalArrows == direction.Down || firstArrow.GetComponent<Enemy>().directionalArrows == direction.reverse_UP || firstArrow.GetComponent<RollingEnemy>().D_Arrow == direction.Down || firstArrow.GetComponent<RollingEnemy>().D_Arrow == direction.reverse_UP)
        {
            firstArrow.RemoveArrow();
            enemyList.RemoveAt(0);
            Score.score += 1;
        }
     }
    public void SwipeLeft()
    {
        Enemy firstArrow = enemyList[0];

        if(firstArrow.GetComponent<Enemy>().directionalArrows == direction.Left || firstArrow.GetComponent<Enemy>().directionalArrows == direction.reverse_Right || firstArrow.GetComponent<RollingEnemy>().D_Arrow == direction.Left || firstArrow.GetComponent<RollingEnemy>().D_Arrow == direction.reverse_Right)
        {
            firstArrow.RemoveArrow();
            enemyList.RemoveAt(0);
             Score.score += 1;
        }
    }
    public void SwipeRight()
    {
        
        Enemy firstArrow = enemyList[0];
        if(firstArrow.GetComponent<Enemy>().directionalArrows == direction.Right || firstArrow.GetComponent<Enemy>().directionalArrows == direction.reverse_Left || firstArrow.GetComponent<RollingEnemy>().D_Arrow == direction.Right || firstArrow.GetComponent<RollingEnemy>().D_Arrow == direction.reverse_Left)
        {
            firstArrow.RemoveArrow();
            enemyList.RemoveAt(0);
             Score.score += 1;
        }
    }

    public void Collide()
    {
        Enemy firstArrow = enemyList[0];
        firstArrow.RemoveArrow();
        enemyList.RemoveAt(0);
    }

    
}
