﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemy : MonoBehaviour
{
    private float speed;
    private float timer = 0f;

    public direction directionalArrows;
    void Start()
    {
       
    }
    
    void Update()
    {
        speed = Random.Range(5f,10f);
        this.transform.position -= this.transform.up*speed*Time.deltaTime;
        timer++;
        if(timer >= 1000)
        {
            //Debug.Log("Erase");
            RemoveArrow();
        }
        
    }

    public void RemoveArrow()
    {
      Debug.Log(this.gameObject.activeSelf);
      this.gameObject.SetActive(false);
    }
    
   
}

public enum direction
    {
      Up,
      Down,
      Right,
      Left,
      reverse_UP,
      reverse_Down,
      reverse_Left,
      reverse_Right,

      None

    }