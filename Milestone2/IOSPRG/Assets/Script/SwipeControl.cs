﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SwipeControl : MonoBehaviour
{
    private bool tap, swipeUp, swipeDown,swipeRight,swipeLeft;
    private bool isDraging = false;
    private bool moved;
    private Vector2 startTouch, swipeDelta;

    public EnemySpawner Spawner;
    //public Transform player;
    public RollingEnemy RNG_Enemy;

    private Vector2 desiredPos;
    private Vector2 dragForce;
    //public float HP = 3;
    public HP lives;

   void Start()
   {
        desiredPos = this.transform.position;
        dragForce =  new Vector2(50f,this.transform.position.y);
        Score.score = 0;
   }

    void Update()
    {
        ResetInput();

        //For Unity Editor
        if (Input.GetMouseButtonDown(0))
        {
            //tap = true;
            isDraging = true;
            moved = false;
            startTouch = Input.mousePosition;

        }
        else if (Input.GetMouseButtonUp(0))
        {
            isDraging = false;
            Reset();
        }
        

       //For mobile/bluestacks
        if (Input.touchCount > 0)
        {
            if (Input.touches[0].phase == TouchPhase.Began)
            {
                //tap = true;
                isDraging = true;
                moved = false;
                startTouch = Input.touches[0].position;
            }
            else if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled)
            {
                isDraging = false;
                Reset();
            }
        }

        swipeDelta = Vector2.zero;
        if (isDraging)
        {
            if (Input.touchCount > 0) // mobile
            {
                swipeDelta = Input.touches[0].position - startTouch;

            }
            else if (Input.GetMouseButton(0)) //unity editor
            {
                swipeDelta = (Vector2)Input.mousePosition - startTouch;
            }
        }

        if (swipeDelta.magnitude > 125)
        {
            moved = true;

            float x = swipeDelta.x;
            float y = swipeDelta.y;
            if(Mathf.Abs(x) > Mathf.Abs(y))
            {
               if (x<0)
               {
                   swipeLeft = true;
               } 
               else 
               {
                   swipeRight = true;
               }
            }
            else
            {
                if (y>0)
                {
                    swipeUp = true;
                }
                else
                {
                    swipeDown = true;
                }
            }
            
            if(Score.score % 100 == 1)
            {
                lives.ExtraHealth();
            }

            Reset();
           
        }

       

        
        if (swipeUp)
        {
            Debug.Log("UpSwipe");
            Spawner.SwipeUP();
            
            
        }

        if (swipeDown)
        {
            Debug.Log("DownSwipe");
            Spawner.SwipeDown();
            
            
        }
        if(swipeLeft)
        {
            Debug.Log("LeftSwipe");
            Spawner.SwipeLeft();
            
        }
        if (swipeRight)
        {
             Debug.Log("RightSwipe");
            Spawner.SwipeRight();
           
        }

        if (tap)
        {
            Debug.Log("Tap");
            this.transform.Translate(dragForce*Time.deltaTime);
        }

    }


    void Reset()
    {
        if (!moved) 
        {
            tap = true;
        }
        startTouch = swipeDelta = Vector2.zero;
        isDraging = false;
    }

    void ResetInput()
    {
        tap = swipeDown = swipeUp = swipeLeft = swipeRight = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
      if(collision.gameObject.CompareTag("Arrow")) 
      {
        Debug.Log("Collide");
        lives.Damage();
       
        Spawner.Collide();

        if(lives.Lives <=0)
        {
          SceneManager.LoadScene("GameOver");
        }
        
      }
    }

}
