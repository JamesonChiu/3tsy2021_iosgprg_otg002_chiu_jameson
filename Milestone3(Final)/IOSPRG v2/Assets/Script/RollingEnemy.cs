﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RollingEnemy : MonoBehaviour
{


    public void SwipeUP()
    {
        GameObject firstArrow = EnemySpawner.Instance.enemyList[0];

        Debug.Log("True");

        if(firstArrow.GetComponent<Enemy>().EnemyArrow == Direction.Up || firstArrow.GetComponent<Enemy>().EnemyArrow == Direction.reverse_Down)
        {
            Debug.Log("UpSwipe");
            Destroy(firstArrow);
            EnemySpawner.Instance.enemyList.RemoveAt(0);
            Score.score += 1;
            KillingSpree.killingSpree += 1;
          
           
        }
       
    }
     public void SwipeDown()
     {
         GameObject firstArrow = EnemySpawner.Instance.enemyList[0];

          Debug.Log("True");


         if(firstArrow.GetComponent<Enemy>().EnemyArrow == Direction.Down || firstArrow.GetComponent<Enemy>().EnemyArrow == Direction.reverse_UP)
        {
             Debug.Log("DownSwipe");
             Destroy(firstArrow);
            EnemySpawner.Instance.enemyList.RemoveAt(0);
            Score.score += 1;
            KillingSpree.killingSpree += 1;
           
        }
        
     }
    public void SwipeLeft()
    {
        GameObject firstArrow = EnemySpawner.Instance.enemyList[0];
         Debug.Log("True");


        if(firstArrow.GetComponent<Enemy>().EnemyArrow == Direction.Left || firstArrow.GetComponent<Enemy>().EnemyArrow == Direction.reverse_Right)
        {
             Debug.Log("SwipeLeft");
            Destroy(firstArrow);
            EnemySpawner.Instance.enemyList.RemoveAt(0);
            Score.score += 1;
            KillingSpree.killingSpree += 1;
           
            
        }
      
    }
    public void SwipeRight()
    {
        
        GameObject firstArrow = EnemySpawner.Instance.enemyList[0];
         Debug.Log("True");

        if(firstArrow.GetComponent<Enemy>().EnemyArrow == Direction.Right || firstArrow.GetComponent<Enemy>().EnemyArrow == Direction.reverse_Left)
        {
             Debug.Log("RightSwipe");
            Destroy(firstArrow);
            EnemySpawner.Instance.enemyList.RemoveAt(0);
            Score.score += 1;
            KillingSpree.killingSpree += 1; 
            
        }
       
    }
}
