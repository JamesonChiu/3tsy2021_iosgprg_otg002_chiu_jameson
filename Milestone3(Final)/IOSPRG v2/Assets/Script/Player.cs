﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    private bool tap, swipeUp, swipeDown,swipeRight,swipeLeft;
    private bool isDraging = false;
    private bool moved;
    private Vector2 startTouch, swipeDelta;

    private Vector2 desiredPos;
    private Vector2 dragForce;

    [SerializeField] bool heal;

    [SerializeField] bool barrier;
    public GameObject sheild;
    public GameObject Boss;

    public int bossTimer = 0;

    
   void Start()
   {
        desiredPos = this.transform.position;
        dragForce =  new Vector2(50f,this.transform.position.y);
        Score.score = 0;
        HP.Lives = 3;
        KillingSpree.killingSpree = 0;
        sheild.SetActive(false);
       EnemySpawner.bossEssence = 0;

       
   }

    void Update()
    {
        ResetInput();

        //For Unity Editor
        if (Input.GetMouseButtonDown(0))
        {
            //tap = true;
            isDraging = true;
            moved = false;
            startTouch = Input.mousePosition;

        }
        else if (Input.GetMouseButtonUp(0))
        {
            isDraging = false;
            Reset();
        }
        

       //For mobile/bluestacks
        if (Input.touchCount > 0)
        {
            if (Input.touches[0].phase == TouchPhase.Began)
            {
                //tap = true;
                isDraging = true;
                moved = false;
                startTouch = Input.touches[0].position;
            }
            else if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled)
            {
                isDraging = false;
                Reset();
            }
        }

        swipeDelta = Vector2.zero;
        if (isDraging)
        {
            if (Input.touchCount > 0) // mobile
            {
                swipeDelta = Input.touches[0].position - startTouch;

            }
            else if (Input.GetMouseButton(0)) //unity editor
            {
                swipeDelta = (Vector2)Input.mousePosition - startTouch;
            }
        }

        if (swipeDelta.magnitude > 125)
        {
            moved = true;

            float x = swipeDelta.x;
            float y = swipeDelta.y;
            if(Mathf.Abs(x) > Mathf.Abs(y))
            {
               if (x<0)
               {
                   swipeLeft = true;
               } 
               else 
               {
                   swipeRight = true;
               }
            }
            else
            {
                if (y>0)
                {
                    swipeUp = true;
                }
                else
                {
                    swipeDown = true;
                }
            }
            
            Reset();
           
        }

        if (swipeUp)
        {
            Debug.Log("UpSwipe");
            EnemySpawner.Instance.SwipeUP();
            
        }
       
        if (swipeDown)
        {
            Debug.Log("DownSwipe");
            EnemySpawner.Instance.SwipeDown();

        }
        
        if(swipeLeft)
        {
            Debug.Log("LeftSwipe");
            EnemySpawner.Instance.SwipeLeft();

        }
       
        if (swipeRight)
        {
             Debug.Log("RightSwipe");
             EnemySpawner.Instance.SwipeRight();
 
        }

        if (tap)
        {
            Debug.Log("Tap");
        }

        //HEAL
        if(Score.score % 10 == 0)
        {
           if(heal)
           {
             heal = false;
             HP.Lives += 1;
             if(HP.Lives > 5)
             {
                 HP.Lives = 5;
             }
           }
        }
        else
        {
            heal = true;
        }

        //Barrier Bonus
        if(KillingSpree.killingSpree >= 5)
        {
           if(!barrier)
           {
              
              barrier = true;
              sheild.SetActive(true);  
           }
        }
        else
        {
           barrier = false;
        }

        //SpawnBoss with 25 kills
        if(EnemySpawner.bossEssence >= 25)
        {
           
            Boss.SetActive(true);
            EnemySpawner.SpawnRNG = 1;
            EnemySpawner.bossEssence = 0;
        }
    }


    void Reset()
    {
        if (!moved) 
        {
            tap = true;
        }
        startTouch = swipeDelta = Vector2.zero;
        isDraging = false;
    }

    void ResetInput()
    {
        tap = swipeDown = swipeUp = swipeLeft = swipeRight = false;
    }

     private void OnTriggerEnter2D(Collider2D collision)
    {
      if(collision.gameObject.CompareTag("Arrow")) 
      {
        Debug.Log("Collide");
        
        EnemySpawner.Instance.Collide();

         if(barrier)
        {
            HP.Lives += 0;
            barrier = false;
            sheild.SetActive(false);
            Debug.Log("remove_sheild");
        }
        else
        {
            HP.Lives -= 1;
        }

        if(HP.Lives <= 0)
        {
            SceneManager.LoadScene("GameOver");
        }

      }
    }
}
