﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HP : MonoBehaviour
{
    public static float Lives = 3;
    Text Health;
    void Start()
    {
       Health = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        Health.text = "Health: " + Lives;
    }

    public void Damage()
    {
        Lives -= 1;
    }

    public void ExtraHealth()
    {
        Lives += 1;
        if(Lives > 5)
        {
            Lives = 5;
        }
        
    }

    public void Immune()
    {
        Lives += 0;
    }
}
