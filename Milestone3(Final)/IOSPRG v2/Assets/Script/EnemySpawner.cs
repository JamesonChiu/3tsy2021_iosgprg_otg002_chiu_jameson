﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
   public static EnemySpawner Instance;
   public List<GameObject> enemyList = new List<GameObject>();
   public GameObject[] E_arrows;

   public static bool isSpawning = true;

   public static int SpawnRNG;

    public static int bossEssence;

    void Start()
    {
        StartCoroutine(Spawn());

        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }

       SpawnRNG = Random.Range(3,5);
    }

    void Update()
    {
      
    }

   void AddToList(GameObject arrows)
   {
       enemyList.Add(arrows);
   }
    
    IEnumerator Spawn()
    {
       while(isSpawning)
       {

           // int SpawnRNG = Random.Range(2,4);
            yield return new WaitForSeconds(SpawnRNG);
            int RNG = Random.Range(0,9);
            GameObject newArrows = Instantiate(E_arrows[RNG], new Vector3(this.transform.position.x,this.transform.position.y,0), Quaternion.identity);
            AddToList(newArrows);  
       }
    }
    public void RemoveArrow(GameObject removeObj)
    {
        enemyList.Remove(removeObj.GetComponent<GameObject>());
    }

   public void SwipeUP()
    {
        GameObject firstArrow = enemyList[0];

        Debug.Log("True");

        if(firstArrow.GetComponent<Enemy>().EnemyArrow == Direction.Up || firstArrow.GetComponent<Enemy>().EnemyArrow == Direction.reverse_Down)
        {
            Debug.Log("UpSwipe");
            Destroy(firstArrow);
            enemyList.RemoveAt(0);
            Score.score += 1;
            KillingSpree.killingSpree += 1;
            bossEssence+=1;
          
           
        }
       
    }
     public void SwipeDown()
     {
         GameObject firstArrow = enemyList[0];

          Debug.Log("True");


         if(firstArrow.GetComponent<Enemy>().EnemyArrow == Direction.Down || firstArrow.GetComponent<Enemy>().EnemyArrow == Direction.reverse_UP)
        {
             Debug.Log("DownSwipe");
             Destroy(firstArrow);
            enemyList.RemoveAt(0);
            Score.score += 1;
            KillingSpree.killingSpree += 1;
            bossEssence+=1;
           
        }
        
     }
    public void SwipeLeft()
    {
        GameObject firstArrow = enemyList[0];
         Debug.Log("True");


        if(firstArrow.GetComponent<Enemy>().EnemyArrow == Direction.Left || firstArrow.GetComponent<Enemy>().EnemyArrow == Direction.reverse_Right)
        {
             Debug.Log("SwipeLeft");
            Destroy(firstArrow);
            enemyList.RemoveAt(0);
            Score.score += 1;
            KillingSpree.killingSpree += 1;
            bossEssence+=1;
           
            
        }
      
    }
    public void SwipeRight()
    {
        
        GameObject firstArrow = enemyList[0];
         Debug.Log("True");

        if(firstArrow.GetComponent<Enemy>().EnemyArrow == Direction.Right || firstArrow.GetComponent<Enemy>().EnemyArrow == Direction.reverse_Left)
        {
             Debug.Log("RightSwipe");
            Destroy(firstArrow);
            enemyList.RemoveAt(0);
            Score.score += 1;
            KillingSpree.killingSpree += 1; 
            bossEssence+=1;
            
        }
       
    }
    public void Collide()
    {
        GameObject firstArrow = enemyList[0];
        Destroy(firstArrow);
        enemyList.RemoveAt(0);
        KillingSpree.killingSpree = 0;
      
    }

    
}
