﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum Direction
    {
      Up,
      Down,
      Right,
      Left,
      reverse_UP,
      reverse_Down,
      reverse_Left,
      reverse_Right,
      None
    }

public enum EnemyType
{
  Normal,
  Rolling
}

public class Enemy : MonoBehaviour
{
    private float speed;
    private float timer = 0f;
    public Direction EnemyArrow;
    
    public EnemyType chosenType;

    [SerializeField] SpriteRenderer RNG_Arrow;
    [SerializeField] Sprite[] Directional_Arrows;

    bool isRolling = true;

    public static int BossHP = 10;
    

    void Start()
    {
      // chosenType = (EnemyType)Random.Range(0,1);
       if(chosenType == EnemyType.Rolling)
       {
         StartCoroutine(EnemyRNG());
         speed = 3f;
       }
    }
    
    void Update()
    {
        speed = Random.Range(5f,10f);
        this.transform.position -= this.transform.up*speed*Time.deltaTime;
        timer++;
        if(timer >= 1000)
        {
            //Debug.Log("Erase");
            RemoveArrow();
        }
        
    }

    public void RemoveArrow()
    {
      Debug.Log(this.gameObject.activeSelf);
      this.gameObject.SetActive(false);
    }
    
     private void OnTriggerEnter2D(Collider2D collision)
    {
      if(collision.gameObject.CompareTag("Player")) 
      {
         EnemySpawner.Instance.Collide();
      }
    }
    
    IEnumerator EnemyRNG()
    {
       
        while(isRolling)
        {
            RNG_Arrow.GetComponent<SpriteRenderer>().sprite = Directional_Arrows[Random.Range(0,Directional_Arrows.Length)];
            SetDirection();
            yield return new WaitForSeconds(.1f);
            StartCoroutine(Stop());
            //yield return new WaitForSeconds(2f);
        }
    }

    IEnumerator Stop()
    {
        yield return new WaitForSeconds(0.5f);
        isRolling = false;
    }

    private void SetDirection()
    {
        if(RNG_Arrow.sprite.name == "Arrow_UP")
        {
           // D_Arrow = RNG_Direction.Up;
           EnemyArrow = Direction.Up;
        }
        else if(RNG_Arrow.sprite.name == "Arrow_DOWN")
        {
           // D_Arrow = RNG_Direction.Down;
           EnemyArrow = Direction.Down;
        }  
        else if(RNG_Arrow.sprite.name == "Arrow_LEFT")
        {
          //  D_Arrow = RNG_Direction.Left;
          EnemyArrow = Direction.Left;
        }  
        else if(RNG_Arrow.sprite.name == "Arrow_RIGHT")
        {
          //  D_Arrow = RNG_Direction.Right;
          EnemyArrow = Direction.Right;
        }
    }
   
}
    


