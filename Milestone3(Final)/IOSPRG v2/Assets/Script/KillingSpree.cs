﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KillingSpree : MonoBehaviour
{
    public static int killingSpree = 0;
    Text SpreePoints;

    // Start is called before the first frame update
    void Start()
    {
        SpreePoints = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        SpreePoints.text = "Killing Spree: " + killingSpree.ToString() + "x";
    }
}
