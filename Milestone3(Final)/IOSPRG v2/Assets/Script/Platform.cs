﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    public static int speed = 5;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position += this.transform.up*speed*Time.deltaTime;
        if(this.transform.position.y < -5f )
        {
            this.transform.position = new Vector3(this.transform.position.x, 4.7f, this.transform.position.z);
        }
    }
}
